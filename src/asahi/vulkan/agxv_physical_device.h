#ifndef AGXV_PHYSICAL_DEVICE
#define AGXV_PHYSICAL_DEVICE 1

#include "agxv_private.h"

#include "vulkan/runtime/vk_physical_device.h"
#include "vk_sync.h"
#include "vk_sync_timeline.h"

#include "asahi/lib/agx_device.h"

struct agxv_instance;

struct agxv_physical_device {
   struct vk_physical_device vk;
   struct agxv_instance *instance;

   struct agx_device dev;

   struct vk_sync_type drm_syncobj_type;
   struct vk_sync_timeline_type sync_timeline_type;
   const struct vk_sync_type *sync_types[3];

   /* Link in agxv_instance::physical_devices */
   struct list_head link;
};

VK_DEFINE_HANDLE_CASTS(agxv_physical_device,
   vk.base,
   VkPhysicalDevice,
   VK_OBJECT_TYPE_PHYSICAL_DEVICE)


VkResult agxv_physical_device_try_create(struct vk_instance *vk_instance,
                                         struct _drmDevice *drm_device,
                                         struct vk_physical_device **out);

void agxv_physical_device_destroy(struct vk_physical_device *);

#endif
