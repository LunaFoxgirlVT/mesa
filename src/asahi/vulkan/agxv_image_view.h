#ifndef AGXV_IMAGE_VIEW
#define AGXV_IMAGE_VIEW 1

#include "agxv_private.h"

#include "asahi/lib/agx_pack.h"

#include "vulkan/runtime/vk_image.h"

struct agxv_image_view {
   struct vk_image_view vk;

   struct agxv_image *image;

   uint8_t prepacked_descriptor[AGX_TEXTURE_LENGTH];
};

VK_DEFINE_HANDLE_CASTS(agxv_image_view, vk.base, VkImageView,
                       VK_OBJECT_TYPE_IMAGE_VIEW)

#endif
