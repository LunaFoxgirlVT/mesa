#ifndef AGXV_DEVICE
#define AGXV_DEVICE 1

#include "agxv_private.h"
#include "agxv_queue.h"

#include "vulkan/runtime/vk_device.h"

#include "asahi/lib/agx_meta.h"

struct agxv_device {
   struct vk_device vk;
   struct agxv_physical_device *pdev;
   struct agxv_queue queue;
   struct agx_meta_cache meta_cache;
};

VK_DEFINE_HANDLE_CASTS(agxv_device, vk.base, VkDevice, VK_OBJECT_TYPE_DEVICE)
#endif
