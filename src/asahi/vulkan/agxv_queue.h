#ifndef AGXV_QUEUE
#define AGXV_QUEUE 1

#include "agxv_private.h"
#include "vk_queue.h"

#include "asahi/lib/agx_device.h"

struct agxv_queue {
   struct vk_queue vk;
   struct agxv_device *device;
   struct agx_command_queue q;
};

VkResult agxv_queue_init(struct agxv_device *device,
                         struct agxv_queue *queue,
                         const VkDeviceQueueCreateInfo *pCreateInfo,
                         uint32_t index_in_family);
#endif
