#include "agxv_physical_device.h"

#include "agxv_entrypoints.h"
#include "agxv_instance.h"

#include "vulkan/runtime/vk_device.h"
#include "vulkan/runtime/vk_sync_dummy.h"
#include "vulkan/wsi/wsi_common.h"

VKAPI_ATTR void VKAPI_CALL
agxv_GetPhysicalDeviceFeatures2(VkPhysicalDevice physicalDevice,
   VkPhysicalDeviceFeatures2 *pFeatures)
{
   // VK_FROM_HANDLE(agxv_physical_device, pdevice, physicalDevice);

   pFeatures->features = (VkPhysicalDeviceFeatures) {
      .robustBufferAccess = true,
      /* More features */
   };

   VkPhysicalDeviceVulkan11Features core_1_1 = {
      .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES,
      /* Vulkan 1.1 features */
   };

   VkPhysicalDeviceVulkan12Features core_1_2 = {
      .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES,
      /* Vulkan 1.2 features */
   };

   VkPhysicalDeviceVulkan13Features core_1_3 = {
      .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES,
      /* Vulkan 1.3 features */
   };

   vk_foreach_struct(ext, pFeatures->pNext)
   {
      if (vk_get_physical_device_core_1_1_feature_ext(ext, &core_1_1))
         continue;
      if (vk_get_physical_device_core_1_2_feature_ext(ext, &core_1_2))
         continue;
      if (vk_get_physical_device_core_1_3_feature_ext(ext, &core_1_3))
         continue;

      switch (ext->sType) {
      case VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_4444_FORMATS_FEATURES_EXT: {
         VkPhysicalDevice4444FormatsFeaturesEXT *features = (void *)ext;
         features->formatA4R4G4B4 = true;
         features->formatA4B4G4R4 = true;
         break;
      }
      /* More feature structs */
      default:
         break;
      }
   }
}

VKAPI_ATTR void VKAPI_CALL
agxv_GetPhysicalDeviceProperties2(VkPhysicalDevice physicalDevice,
   VkPhysicalDeviceProperties2 *pProperties)
{
   VK_FROM_HANDLE(agxv_physical_device, pdevice, physicalDevice);

   pProperties->properties = (VkPhysicalDeviceProperties) {
      .apiVersion = VK_MAKE_VERSION(1, 0, VK_HEADER_VERSION),
      .driverVersion = vk_get_driver_version(),
      .deviceType = VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU,
      .deviceID = pdevice->dev.id.chip_id,
      .vendorID = 0x160B,
      .limits = (VkPhysicalDeviceLimits) {
         .maxViewports = 1,
      }
      /* More properties */
   };
   strncpy(pProperties->properties.deviceName, pdevice->dev.id.name,
           sizeof(pdevice->dev.id.name));

   VkPhysicalDeviceVulkan11Properties core_1_1 = {
      .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_PROPERTIES,
      /* Vulkan 1.1 properties */
   };

   VkPhysicalDeviceVulkan12Properties core_1_2 = {
      .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_PROPERTIES,
      /* Vulkan 1.2 properties */
   };

   VkPhysicalDeviceVulkan13Properties core_1_3 = {
      .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_PROPERTIES,
      /* Vulkan 1.3 properties */
   };

   vk_foreach_struct(ext, pProperties->pNext)
   {
      if (vk_get_physical_device_core_1_1_property_ext(ext, &core_1_1))
         continue;
      if (vk_get_physical_device_core_1_2_property_ext(ext, &core_1_2))
         continue;
      if (vk_get_physical_device_core_1_3_property_ext(ext, &core_1_3))
         continue;

      switch (ext->sType) {
      /* More property structs */
      default:
         break;
      }
   }
}

PUBLIC VKAPI_ATTR PFN_vkVoidFunction VKAPI_CALL
vk_icdGetPhysicalDeviceProcAddr(VkInstance _instance, const char *pName)
{
   VK_FROM_HANDLE(agxv_instance, instance, _instance);
   return vk_instance_get_physical_device_proc_addr(&instance->vk, pName);
}

static void
agxv_get_device_extensions(const struct agxv_physical_device *device,
   struct vk_device_extension_table *ext)
{
   *ext = (struct vk_device_extension_table) {
      .KHR_copy_commands2 = true,
      .KHR_variable_pointers = true,
   };
}

VkResult
agxv_physical_device_try_create(struct vk_instance *vk_instance,
                                struct _drmDevice *drm_device,
                                struct vk_physical_device **out)
{
   struct agxv_instance *instance =
      container_of(vk_instance, struct agxv_instance, vk);
   // const char *primary_path = drm_device->nodes[DRM_NODE_PRIMARY];
   const char *path = drm_device->nodes[DRM_NODE_RENDER];
   VkResult result;
   int fd;

   if (!(drm_device->available_nodes & (1 << DRM_NODE_RENDER)) ||
      drm_device->bustype != DRM_BUS_PLATFORM) {
      return VK_ERROR_INCOMPATIBLE_DRIVER;
   }

   fd = open(path, O_RDWR | O_CLOEXEC);
   if (fd < 0) {
      if (errno == ENOMEM) {
         return vk_errorf(
            instance, VK_ERROR_OUT_OF_HOST_MEMORY, "Unable to open device %s: out of memory", path);
      }
      return vk_errorf(
         instance, VK_ERROR_INCOMPATIBLE_DRIVER, "Unable to open device %s: %m", path);
   }

   vk_warn_non_conformant_implementation("agxv");

   struct agxv_physical_device *device =
      vk_zalloc(&instance->vk.alloc, sizeof(*device), 8, VK_SYSTEM_ALLOCATION_SCOPE_INSTANCE);

   if (device == NULL) {
      result = vk_error(instance, VK_ERROR_OUT_OF_HOST_MEMORY);
      goto fail_fd;
   }

   struct vk_physical_device_dispatch_table dispatch_table;
   vk_physical_device_dispatch_table_from_entrypoints(
      &dispatch_table, &agxv_physical_device_entrypoints, true);
   vk_physical_device_dispatch_table_from_entrypoints(
      &dispatch_table, &wsi_physical_device_entrypoints, false);

   struct vk_device_extension_table supported_extensions;
   agxv_get_device_extensions(device, &supported_extensions);

   result =
      vk_physical_device_init(&device->vk, &instance->vk, &supported_extensions, &dispatch_table);

   if (result != VK_SUCCESS) {
      vk_error(instance, result);
      goto fail_alloc;
   }

   device->dev.debug |= AGX_DBG_TRACE;
   device->dev.fd = fd;

   if (!agx_open_device(NULL, &device->dev)) {
      result = VK_ERROR_INITIALIZATION_FAILED;
      vk_error(instance, result);
      goto fail_alloc;
   }

   device->instance = instance;

   device->drm_syncobj_type = vk_sync_dummy_type;
   device->drm_syncobj_type.features &= ~VK_SYNC_FEATURE_TIMELINE;
   device->sync_timeline_type =
      vk_sync_timeline_get_type(&device->drm_syncobj_type);

   device->sync_types[0] =
      &device->drm_syncobj_type;
   device->sync_types[1] =
      &device->sync_timeline_type.sync;
   device->sync_types[2] = NULL;
   device->vk.supported_sync_types =
      device->sync_types;

   *out = &device->vk;

   return VK_SUCCESS;

fail_alloc:
   vk_free(&instance->vk.alloc, device);

fail_fd:
   close(fd);
   return result;
}

void
agxv_physical_device_destroy(struct vk_physical_device *vk_device)
{
   struct agxv_physical_device *device = container_of(vk_device, struct agxv_physical_device, vk);
   vk_free(&device->instance->vk.alloc, device);
}

VKAPI_ATTR void VKAPI_CALL
agxv_GetPhysicalDeviceMemoryProperties2(VkPhysicalDevice physicalDevice,
   VkPhysicalDeviceMemoryProperties2 *pMemoryProperties)
{
   pMemoryProperties->memoryProperties = (VkPhysicalDeviceMemoryProperties){
      .memoryTypeCount = 1,
      .memoryTypes[0].heapIndex = 0,
      .memoryTypes[0].propertyFlags =
         VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT |
         VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
         VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
      .memoryHeapCount = 1,
      /* FIXME: there is no portable way to get memory size. We could use
       * sysinfo(2) but that would be Linux specific. For now we just specify
       * 8GB as there are no Apple Silicon devices with less RAM than this.
       */
      .memoryHeaps[0].size = 8 * 1024 * 1024 * 1024
   };
   vk_foreach_struct(ext, pMemoryProperties->pNext)
   {
      switch (ext->sType) {
      default:
         agxv_debug_ignored_stype(ext->sType);
         break;
      }
   }
}

VKAPI_ATTR void VKAPI_CALL
agxv_GetPhysicalDeviceQueueFamilyProperties2(VkPhysicalDevice physicalDevice,
   uint32_t *pQueueFamilyPropertyCount,
   VkQueueFamilyProperties2 *pQueueFamilyProperties)
{
   // VK_FROM_HANDLE(agxv_physical_device, pdevice, physicalDevice);
   VK_OUTARRAY_MAKE_TYPED(
      VkQueueFamilyProperties2, out, pQueueFamilyProperties, pQueueFamilyPropertyCount);

   vk_outarray_append_typed(VkQueueFamilyProperties2, &out, p) {
      p->queueFamilyProperties.queueFlags =
         VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT;
      p->queueFamilyProperties.queueCount = 1;
      p->queueFamilyProperties.timestampValidBits = 64;
      p->queueFamilyProperties.minImageTransferGranularity =
         (VkExtent3D) { 1, 1, 1 };
   }
}

VKAPI_ATTR void VKAPI_CALL
agxv_GetPhysicalDeviceFormatProperties2(VkPhysicalDevice physicalDevice,
   VkFormat vk_format,
   VkFormatProperties2 *pFormatProperties)
{
   vk_foreach_struct(ext, pFormatProperties->pNext)
   {
      /* Use unsigned since some cases are not in the VkStructureType enum. */
      switch ((unsigned)ext->sType) {
      default:
         agxv_debug_ignored_stype(ext->sType);
         break;
      }
   }
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_GetPhysicalDeviceImageFormatProperties2(VkPhysicalDevice physicalDevice,
   const VkPhysicalDeviceImageFormatInfo2 *base_info,
   VkImageFormatProperties2 *base_props)
{
   //return VK_ERROR_FORMAT_NOT_SUPPORTED;
   return VK_SUCCESS;
}
