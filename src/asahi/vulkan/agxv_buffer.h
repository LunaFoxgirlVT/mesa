#ifndef AGXV_BUFFER
#define AGXV_BUFFER 1

#include "agxv_private.h"

#include "agxv_device_memory.h"

#include "vulkan/runtime/vk_buffer.h"

struct agxv_buffer {
   struct vk_buffer vk;
   struct agxv_device_memory *mem;
   uint64_t mem_offset;
};

static inline uint64_t
agxv_buffer_addr(struct agxv_buffer *buffer) {
   return buffer->mem->bo->ptr.gpu + buffer->mem_offset;
}

VK_DEFINE_HANDLE_CASTS(agxv_buffer, vk.base, VkBuffer, VK_OBJECT_TYPE_BUFFER)

#endif
