#include "agxv_descriptor_set.h"

#include "agxv_buffer.h"
#include "agxv_device.h"
#include "agxv_device_memory.h"
#include "agxv_descriptor_pool.h"
#include "agxv_descriptor_set_layout.h"
#include "asahi/lib/agx_bo.h"

static VkResult
agxv_descriptor_set_create(struct agxv_device *device,
                           struct agxv_descriptor_pool *pool,
                           struct agxv_descriptor_set_layout *layout,
                           VkDescriptorSet *pDescriptorSet)
{
   uint64_t offset = pool->current_offset;

   if (offset + layout->size > pool->size)
      return vk_error(pool, VK_ERROR_OUT_OF_POOL_MEMORY);

   struct agxv_descriptor_set *set;

   set = vk_object_zalloc(&device->vk, NULL, sizeof(*set),
                          VK_OBJECT_TYPE_DESCRIPTOR_SET);
   if (!set)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   vk_descriptor_set_layout_ref(&layout->vk);
   set->pool = pool;
   set->layout = layout;

   for (; set->entry < pool->entry_count; set->entry++) {
      if (!pool->entries[set->entry].size)
         break;
   }

   if (set->entry == pool->entry_count) {
      vk_free(NULL, set);
      return vk_error(pool, VK_ERROR_OUT_OF_POOL_MEMORY);
   }

   pool->entries[set->entry].offset = offset;
   pool->entries[set->entry].size = layout->size;

   pool->current_offset += layout->size;

   *pDescriptorSet = agxv_descriptor_set_to_handle(set);

   return VK_SUCCESS;
}

static void
agxv_descriptor_set_destroy(struct agxv_device *device,
                            struct agxv_descriptor_pool *pool,
                            struct agxv_descriptor_set *set)
{
   uint64_t offset = pool->entries[set->entry].offset;
   uint64_t size = pool->entries[set->entry].size;

   pool->entries[set->entry].size = 0;

   memmove(pool->bo->ptr.cpu + offset,
           pool->bo->ptr.cpu + offset + size,
           pool->size - offset - size);

   for (uint32_t i = 0; i < pool->entry_count; i++) {
      struct agxv_descriptor_pool_entry *entry = &pool->entries[i];
      if (!entry->size)
         continue;
      if (entry->offset < offset)
         continue;
      entry->offset -= size;
   }

   pool->current_offset -= size;
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_AllocateDescriptorSets(VkDevice _device,
                            const VkDescriptorSetAllocateInfo *pAllocateInfo,
                            VkDescriptorSet *pDescriptorSets)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_descriptor_pool, pool, pAllocateInfo->descriptorPool);

   VkResult result = VK_SUCCESS;

   uint32_t i;
   for (i = 0; i < pAllocateInfo->descriptorSetCount; i++) {
      VK_FROM_HANDLE(agxv_descriptor_set_layout, layout, pAllocateInfo->pSetLayouts[i]);
      result = agxv_descriptor_set_create(device,
                                          pool,
                                          layout,
                                          &pDescriptorSets[i]);
      if (result != VK_SUCCESS)
         break;
   }

   if (result != VK_SUCCESS) {
     agxv_FreeDescriptorSets(_device, pAllocateInfo->descriptorPool,
                          i, pDescriptorSets);
     for (i = 0; i < pAllocateInfo->descriptorSetCount; i++)
        pDescriptorSets[i] = VK_NULL_HANDLE;
   }

   return result;
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_FreeDescriptorSets(VkDevice _device,
                        VkDescriptorPool descriptorPool,
                        uint32_t descriptorSetCount,
                        const VkDescriptorSet *pDescriptorSets)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_descriptor_pool, pool, descriptorPool);

   for (uint32_t i = 0; i < descriptorSetCount; i++) {
      VK_FROM_HANDLE(agxv_descriptor_set, set, pDescriptorSets[i]);

      if (set)
         agxv_descriptor_set_destroy(device, pool, set);
   }

   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_UpdateDescriptorSets(VkDevice _device,
                          uint32_t descriptorWriteCount,
                          const VkWriteDescriptorSet *pDescriptorWrites,
                          uint32_t descriptorCopyCount,
                          const VkCopyDescriptorSet *pDescriptorCopies)
{
   for (uint32_t i = 0; i < descriptorWriteCount; i++) {
      const VkWriteDescriptorSet *write = &pDescriptorWrites[i];
      VK_FROM_HANDLE(agxv_descriptor_set, set, write->dstSet);
      uint64_t offset = set->pool->entries[set->entry].offset;
      /* offset += set->map[binding]; */
      /* offset += get_descriptor_size(write->descriptorType); * write->dstArrayElement */
      switch (write->descriptorType) {
      case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
      case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER: {
         VK_FROM_HANDLE(agxv_buffer, buffer, write->pBufferInfo->buffer);
         struct agxv_buffer_descriptor desc = {
            .ptr = agxv_buffer_addr(buffer) + write->pBufferInfo->offset,
         };
         memcpy(set->pool->bo->ptr.cpu + offset, &desc, sizeof(desc));
         break;
      }
      default:
         assert(!"Unhandled descriptor type");
      }
   }
}
