#ifndef AGXV_DESCRIPTOR_SET_LAYOUT
#define AGXV_DESCRIPTOR_SET_LAYOUT 1

#include "agxv_private.h"

#include "vulkan/runtime/vk_descriptor_set_layout.h"

struct agxv_descriptor_set_layout_binding {
   VkDescriptorType type;

   /* Number of array elements in this binding */
   uint32_t array_size;

   /* Offset into the descriptor buffer */
   uint32_t offset;

   /* Number of bytes between each array element */
   uint8_t stride;
};

struct agxv_descriptor_set_layout {
   struct vk_descriptor_set_layout vk;

   /* Total size for this layout */
   uint32_t size;

   uint32_t binding_count;
   struct agxv_descriptor_set_layout_binding bindings[];
};

VK_DEFINE_HANDLE_CASTS(agxv_descriptor_set_layout, vk.base,
                       VkDescriptorSetLayout,
                       VK_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT);

/* FIXME: is this the right place for this? */
static inline uint32_t
agxv_binding_stride(VkDescriptorType type)
{
   switch (type) {
   case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
   case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER:
      /* TODO: don't hard code
       * 64bit global 32bit offset
       */
      return sizeof(uint64_t) * 2;
   default:
      unreachable("Unsupported binding type");
   }
}

#endif
