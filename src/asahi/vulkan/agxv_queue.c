#include "agxv_queue.h"

#include "agxv_cmd_buffer.h"
#include "agxv_device.h"
#include "agxv_physical_device.h"

#include "asahi/lib/decode.h"

static VkResult
submit_cmd_buffer(struct agxv_queue *queue,
                  struct agxv_cmd_buffer *cmd_buffer)
{
   list_for_each_entry_safe(struct agxv_job, job,
                            &cmd_buffer->jobs, link) {
      struct drm_asahi_submit submit = {
         .queue_id = queue->q.id
      };
      switch (job->type) {
      case AGXV_JOB_GRAPHICS:
         submit.cmd_type = DRM_ASAHI_CMD_RENDER;
         submit.cmd_buffer = (uintptr_t) &job->render_buf;
         break;
      case AGXV_JOB_COMPUTE:
         submit.cmd_type = DRM_ASAHI_CMD_COMPUTE;
         submit.cmd_buffer = (uintptr_t) &job->compute_buf;
         break;
      };
      int fd = cmd_buffer->device->pdev->dev.fd;

      int ret = drmIoctl(fd, DRM_IOCTL_ASAHI_SUBMIT, &submit);
      if (ret) {
         return vk_errorf(queue, VK_ERROR_DEVICE_LOST,
                          "DRM_IOCTL_ASAHI_SUBMIT failed: %m\n");
      }
   }

   return VK_SUCCESS;
}

static VkResult
agxv_queue_submit(struct vk_queue *vk_queue,
                  struct vk_queue_submit *submit)
{
   struct agxv_queue *queue = container_of(vk_queue, struct agxv_queue, vk);
   struct agx_device *dev = &queue->device->pdev->dev;

   VkResult result = VK_SUCCESS;

   for (unsigned i = 0; i < submit->command_buffer_count; i++) {
      struct agxv_cmd_buffer *cmd_buffer =
         container_of(submit->command_buffers[i], struct agxv_cmd_buffer, vk);
      result = submit_cmd_buffer(queue, cmd_buffer);

      if (result != VK_SUCCESS)
         return result;
   };

   return VK_SUCCESS;
}

VkResult agxv_queue_init(struct agxv_device *device,
                         struct agxv_queue *queue,
                         const VkDeviceQueueCreateInfo *pCreateInfo,
                         uint32_t index_in_family)
{
   VkResult result;

   result = vk_queue_init(&queue->vk, &device->vk,
                          pCreateInfo, index_in_family);
   if (result != VK_SUCCESS)
      return result;

   queue->vk.driver_submit = agxv_queue_submit;
   queue->device = device;

   enum drm_asahi_queue_type q_type[] = {
      [0] = DRM_ASAHI_QUEUE_RENDER,
      [1] = DRM_ASAHI_QUEUE_COMPUTE
   };

   // queue->q = agx_create_command_queue(&device->pdev->dev, q_type[pCreateInfo->queueFamilyIndex]);
   queue->q = agx_create_command_queue(&device->pdev->dev, q_type[1]);
   return VK_SUCCESS;
}
