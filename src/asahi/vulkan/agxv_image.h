#ifndef AGXV_IMAGE
#define AGXV_IMAGE 1

#include "agxv_private.h"

#include "agxv_device_memory.h"

#include "asahi/layout/layout.h"

#include "vulkan/runtime/vk_image.h"

struct agxv_image {
   struct vk_image vk;
   struct ail_layout layout;
   struct agxv_device_memory *mem;
   VkDeviceSize offset;
};

static inline uint64_t
agxv_image_addr(struct agxv_image *image) {
   return image->mem->bo->ptr.gpu + image->offset;
}

VK_DEFINE_HANDLE_CASTS(agxv_image, vk.base, VkImage, VK_OBJECT_TYPE_IMAGE)

#endif
