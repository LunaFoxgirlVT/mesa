#include "agxv_pipeline.h"

#include "agxv_physical_device.h"
#include "compiler/nir/nir_builder.h"
#include "vk_shader_module.h"
#include "vk_pipeline_layout.h"
#include "compiler/spirv/nir_spirv.h"
#include "asahi/lib/agx_nir_lower_vbo.h"


VKAPI_ATTR void VKAPI_CALL
agxv_DestroyPipeline(VkDevice _device,
                     VkPipeline _pipeline,
                     const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_pipeline, pipeline, _pipeline);

   if (pipeline->destroy)
      pipeline->destroy(pipeline);

   vk_object_free(&device->vk, pAllocator, pipeline);
}

/* Register layout is as follows:
 *  - push constants
 *  - sets
 *  - vertex buffers (if vertex input)
 *
 * Buffers must come last in order to maintain pipeline layout compatibility as
 * the number of buffers can change depending on pipeline. Push constant length
 * is constant across pipelines created with the same pipeline layout. The
 * number of sets can be larger for pipelines with a compatible layout. We only
 * need to bind the sets up to the vertex buffer base for the pipeline to
 * maintain compatibility.
 */
struct agxv_uniform_reg_layout {
   uint32_t push_base;
   uint32_t set_base;
   uint32_t buffer_base;
};

/* emit a ubo load to get the address of the binding
 * TODO:
 * This isn't correct for inline ubo, sampler, image and combined
 * descriptors. For those cases the address should be returned.
 */
static void
lower_load_vulkan_descriptor(nir_builder *b,
                             nir_intrinsic_instr *instr,
                             nir_shader *shader,
                             struct agxv_uniform_reg_layout *layout)
{
   assert(instr->intrinsic == nir_intrinsic_load_vulkan_descriptor);
   nir_intrinsic_instr *parent = nir_src_as_intrinsic(instr->src[0]);
   assert(parent->intrinsic == nir_intrinsic_vulkan_resource_index);
   unsigned set = nir_intrinsic_desc_set(parent);
   unsigned binding = nir_intrinsic_binding(parent);

   unsigned register_index = layout->set_base + set * 4;

   b->cursor = nir_before_instr(&instr->instr);
   nir_ssa_def *set_addr =
     nir_load_preamble(b, 1, 64, .base = register_index);
   /* TODO: get binding offset from set layout */
   nir_ssa_def *binding_offset = nir_imm_int(b, binding * 16);
   nir_ssa_def *binding_data =
      nir_build_load_global_constant_offset(b, 4, nir_dest_bit_size(instr->dest),
                                            set_addr, binding_offset);

   nir_ssa_def_rewrite_uses(&instr->dest.ssa,
                            binding_data);
   nir_instr_remove(&instr->instr);
}

static void
lower_load_vbo_base_agx(nir_builder *b,
                        nir_intrinsic_instr *instr,
                        nir_shader *shader,
                        struct agxv_uniform_reg_layout *layout)
{
   assert(instr->intrinsic == nir_intrinsic_load_vbo_base_agx);
   /* TODO: calculate properly with pipeline layout */
   uint32_t offset = 4 + 4 * nir_src_as_uint( instr->src[0]);
   nir_ssa_def *value =
      nir_load_preamble(b, 1, nir_dest_bit_size(instr->dest), .base = offset);
   nir_ssa_def_rewrite_uses(&instr->dest.ssa, value);
   nir_instr_remove(&instr->instr);
}

static bool
lower_intrinsic(nir_builder *b,
                nir_intrinsic_instr *instr,
                nir_shader *shader,
                struct agxv_uniform_reg_layout *layout)
{
   bool progress = false;

   switch (instr->intrinsic) {
   case nir_intrinsic_load_vulkan_descriptor:
      lower_load_vulkan_descriptor(b, instr, shader, layout);
      progress |= true;
      break;
   case nir_intrinsic_load_vbo_base_agx:
      lower_load_vbo_base_agx(b, instr, shader, layout);
      progress |= true;
      break;
   default:
      break;
   }

   return progress;
}

static bool
lower_impl(nir_function_impl *impl,
           nir_shader *shader,
           struct agxv_uniform_reg_layout *layout)
{
   nir_builder b;
   nir_builder_init(&b, impl);
   bool progress = false;

   nir_foreach_block(block, impl) {
      nir_foreach_instr_safe(instr, block) {
         b.cursor = nir_before_instr(instr);
         switch (instr->type) {
         case nir_instr_type_intrinsic:
            progress |= lower_intrinsic(&b,
                                        nir_instr_as_intrinsic(instr),
                                        shader, layout);
            break;
         default:
            break;
         }
      }
   }

   return progress;
}

static bool
lower_pipeline_layout(nir_shader *shader,
                      struct agxv_uniform_reg_layout *layout)
{
   bool progress = false;
   nir_foreach_function(function, shader) {
      if (function->impl)
         progress |= lower_impl(function->impl,
                                shader, layout);
   }
   return progress;
}

static bool
lower_load_global_constant_offset_instr(nir_builder *b, nir_instr *instr,
                                        UNUSED void *_data)
{
   if (instr->type != nir_instr_type_intrinsic)
      return false;

   nir_intrinsic_instr *intrin = nir_instr_as_intrinsic(instr);
   if (intrin->intrinsic != nir_intrinsic_load_global_constant_offset &&
       (intrin->intrinsic != nir_intrinsic_load_global_constant_bounded || true))
      return false;

   b->cursor = nir_before_instr(&intrin->instr);

   nir_ssa_def *base_addr = intrin->src[0].ssa;
   nir_ssa_def *offset = intrin->src[1].ssa;

   nir_ssa_def *val =
      nir_build_load_global_constant(b, intrin->dest.ssa.num_components,
                            intrin->dest.ssa.bit_size,
                            nir_iadd(b, base_addr, nir_u2u64(b, offset)),
                            .access = nir_intrinsic_access(intrin),
                            .align_mul = nir_intrinsic_align_mul(intrin),
                            .align_offset = nir_intrinsic_align_offset(intrin));

   nir_ssa_def_rewrite_uses(&intrin->dest.ssa, val);

   return true;
}

static const struct spirv_to_nir_options default_spirv_options =  {
   .caps = {
    },
   .ubo_addr_format = nir_address_format_64bit_global_32bit_offset,
   .ssbo_addr_format = nir_address_format_64bit_global_32bit_offset,
   .push_const_addr_format = nir_address_format_32bit_offset
};

VkResult
agxv_compile_pipeline_stage(struct agxv_device *device,
                            struct agxv_pipeline *pipeline,
                            struct vk_pipeline_layout *layout,
                            const VkPipelineShaderStageCreateInfo *pCreateInfo,
                            void (*stage_specific_lowering)(struct agxv_pipeline *pipeline,
                                                            nir_shader *nir,
                                                            struct agx_shader_key *, void *),
                            void *data)
{
   VkResult result = VK_SUCCESS;
   VK_FROM_HANDLE(vk_shader_module, mod, pCreateInfo->module);
   gl_shader_stage stage = vk_to_mesa_shader_stage(pCreateInfo->stage);

   struct agx_shader_info *info = &pipeline->stages[stage].info;

   nir_shader *nir;
   result = vk_shader_module_to_nir(&device->vk, mod, stage,
                                    pCreateInfo->pName,
                                    pCreateInfo->pSpecializationInfo,
                                    &default_spirv_options,
                                    &agx_nir_options,
                                    NULL, &nir);

   struct util_dynarray bin;
   util_dynarray_init(&bin, NULL);

   struct agxv_uniform_reg_layout reg_layout = {
      .push_base = 0,
      .set_base = 0, /* TODO: implement push constants */
      .buffer_base = 4 * layout->set_count
   };

   nir_assign_io_var_locations(nir, nir_var_shader_in,
                               &nir->num_inputs, stage);
   nir_assign_io_var_locations(nir, nir_var_shader_out,
                               &nir->num_outputs, stage);
   agx_preprocess_nir(nir, false);
   NIR_PASS_V(nir, nir_lower_system_values);
   NIR_PASS_V(nir, nir_lower_explicit_io,
              nir_var_mem_ubo | nir_var_mem_ssbo,
              nir_address_format_64bit_global_32bit_offset);

   /* TODO: callbacks */
   struct agx_shader_key key = {
      .reserved_preamble = reg_layout.buffer_base
   };

   stage_specific_lowering(pipeline, nir, &key, data);

   NIR_PASS_V(nir, lower_pipeline_layout, &reg_layout);
   NIR_PASS(_, nir, nir_shader_instructions_pass,
            lower_load_global_constant_offset_instr,
            nir_metadata_block_index | nir_metadata_dominance, NULL);

   agx_compile_shader_nir(nir, &key, NULL, &bin, info);

   if (result != VK_SUCCESS)
      return result;
   pipeline->stages[stage].bo = agx_bo_create(&device->pdev->dev,
                                                   bin.size,
                                                   AGX_BO_LOW_VA | AGX_BO_EXEC,
                                                   "Shader");
   memcpy(pipeline->stages[stage].bo->ptr.cpu, bin.data, bin.size);

   return result;
}
