#include "agxv_cmd_buffer.h"

#include "agxv_buffer.h"
#include "agxv_device.h"
#include "agxv_physical_device.h"
#include "agxv_graphics_pipeline.h"
#include "agxv_descriptor_set.h"
#include "agxv_image.h"
#include "agxv_image_view.h"

#include "asahi/lib/decode.h"
#include "asahi/lib/agx_ppp.h"
#include "asahi/lib/agx_meta.h"
#include "asahi/lib/agx_usc.h"

#include "vk_command_pool.h"
#include "vk_format.h"
#include "vk_pipeline_layout.h"

extern const struct vk_command_buffer_ops agxv_cmd_buffer_ops;

static VkResult
agxv_cmd_buffer_create(struct vk_command_pool *pool,
                       struct vk_command_buffer **vk_cmd_buffer)
{
   VkResult result;

   struct agxv_cmd_buffer *cmd_buffer;
   cmd_buffer = vk_zalloc(&pool->alloc, sizeof(*cmd_buffer), 8,
                          VK_SYSTEM_ALLOCATION_SCOPE_OBJECT);

   if (!cmd_buffer)
      return vk_error(pool->base.device, VK_ERROR_OUT_OF_HOST_MEMORY);

   result = vk_command_buffer_init(pool, &cmd_buffer->vk,
                                   &agxv_cmd_buffer_ops, 0);

   if (result != VK_SUCCESS) {
      vk_free(&pool->alloc, cmd_buffer);
      return result;
   }

   list_inithead(&cmd_buffer->jobs);

   cmd_buffer->device = container_of(pool->base.device,
                                     struct agxv_device,
                                     vk);

   *vk_cmd_buffer = &cmd_buffer->vk;

   return VK_SUCCESS;
}

static void
agxv_cmd_buffer_reset(struct vk_command_buffer *vk_cmd_buffer,
                      VkCommandBufferResetFlags flags)
{
   struct agxv_cmd_buffer *cmd_buffer =
      container_of(vk_cmd_buffer, struct agxv_cmd_buffer, vk);

   agx_pool_cleanup(&cmd_buffer->cmd_pool);
   agx_pool_cleanup(&cmd_buffer->pipeline_pool);
   agx_pool_cleanup(&cmd_buffer->shader_pool);

   vk_command_buffer_reset(vk_cmd_buffer);
}

static void
agxv_cmd_buffer_destroy(struct vk_command_buffer *vk_cmd_buffer)
{
   agxv_cmd_buffer_reset(vk_cmd_buffer, 0);
   /* TODO: agxv_cmd_buffer::pool::alloc */
   vk_command_buffer_finish(vk_cmd_buffer);
   vk_free(&vk_cmd_buffer->pool->alloc, vk_cmd_buffer);
}

const struct vk_command_buffer_ops agxv_cmd_buffer_ops = {
   .create = agxv_cmd_buffer_create,
   .reset = agxv_cmd_buffer_reset,
   .destroy = agxv_cmd_buffer_destroy,
};

static struct agxv_job *
agxv_job_create(struct agxv_cmd_buffer *cmd_buffer,
                enum agxv_job_type type)
{
   struct agxv_job *job;
   job = vk_zalloc(&cmd_buffer->device->vk.alloc, sizeof(*job), 1,
                   VK_SYSTEM_ALLOCATION_SCOPE_COMMAND);

   job->type = type;

   list_addtail(&job->link, &cmd_buffer->jobs);


   /* Allocate the command buffer a 64k at a time; When we run out of
    * space, we insert a stream link.
    */
   cmd_buffer->state.encoder = agx_pool_alloc_aligned(&cmd_buffer->cmd_pool,
                                                      0x10000,
                                                      0x4000);
   cmd_buffer->state.encoder_current = cmd_buffer->state.encoder.cpu;


   if (type == AGXV_JOB_GRAPHICS) {
      job->render_buf.flags = 0;
      job->render_buf.flags |= ASAHI_CMDBUF_NO_CLEAR_PIPELINE_TEXTURES;
      /* FIXME: do we need to do this on LOAD_OP_NONE or LOAD_OP_DONT_CARE? */
      job->render_buf.flags |= ASAHI_CMDBUF_PROCESS_EMPTY_TILES;
      job->render_buf.encoder_ptr = cmd_buffer->state.encoder.gpu;
      job->render_buf.encoder_id = agx_get_global_id(&cmd_buffer->device->pdev->dev);
      job->render_buf.cmd_3d_id = agx_get_global_id(&cmd_buffer->device->pdev->dev);
      job->render_buf.cmd_ta_id = agx_get_global_id(&cmd_buffer->device->pdev->dev);

      job->render_buf.ppp_ctrl = 0x203;
      job->render_buf.ppp_multisamplectl = 0x88;
      agx_pack(&job->render_buf.zls_ctrl, ZLS_CONTROL, cfg) {
         cfg.z_load_enable = false;
         cfg.z_store_enable = false;
         cfg.s_load_enable = false;
         cfg.s_store_enable = false;
      };

      job->render_buf.samples = 1;
      job->render_buf.layers = 1;

      job->render_buf.fb_width = 0;
      job->render_buf.fb_height = 0;

      job->render_buf.load_pipeline_bind = 0xffff8002;

      job->render_buf.store_pipeline_bind = 0x12;
      job->render_buf.store_pipeline = 0;

      job->render_buf.partial_reload_pipeline_bind = 0xffff8212;
      job->render_buf.partial_reload_pipeline = 0;

      job->render_buf.partial_store_pipeline_bind = 0x12;
      job->render_buf.partial_store_pipeline = 0;

      job->render_buf.attachment_count = 0;
   } else if (type == AGXV_JOB_COMPUTE) {
      job->compute_buf.encoder_ptr = cmd_buffer->state.encoder.gpu;
      job->compute_buf.encoder_id = agx_get_global_id(&cmd_buffer->device->pdev->dev);
      job->compute_buf.iogpu_unk_40 = 0x1c;
      job->compute_buf.cmd_id = agx_get_global_id(&cmd_buffer->device->pdev->dev);
      job->compute_buf.iogpu_unk_44 = 0xffffffff;
   } else {
      __builtin_trap();
   }
   return job;
}

static void
agxv_job_destroy(struct agxv_job *job)
{
   switch (job->type) {
   case AGXV_JOB_GRAPHICS:
      break;
   default:
      unreachable("unhandled job type");
   }
   vk_free(NULL, job);
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_BeginCommandBuffer(VkCommandBuffer commandBuffer,
                        const VkCommandBufferBeginInfo *pBeginInfo)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   agxv_cmd_buffer_reset(&cmd_buffer->vk, 0);

   agx_pool_init(&cmd_buffer->cmd_pool,
                 &cmd_buffer->device->pdev->dev,
                 AGX_BO_WRITEBACK,
                 false);

   agx_pool_init(&cmd_buffer->pipeline_pool,
                 &cmd_buffer->device->pdev->dev,
                 AGX_BO_LOW_VA,
                 false);

   agx_pool_init(&cmd_buffer->shader_pool,
                 &cmd_buffer->device->pdev->dev,
                 AGX_BO_EXEC | AGX_BO_LOW_VA,
                 false);

   return VK_SUCCESS;
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_EndCommandBuffer(VkCommandBuffer commandBuffer)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   return vk_command_buffer_get_record_result(&cmd_buffer->vk);
}

static struct agx_pixel_format_entry
agx_pixel_format_from_vk(VkFormat format) {
   enum pipe_format pipe = vk_format_to_pipe_format(format);
   return agx_pixel_format[pipe];
}

static uint64_t
agxv_pack_rt(struct agxv_cmd_buffer *cmd_buffer,
             const VkRenderingAttachmentInfo *attachment)
{
   struct agx_ptr ret = agx_pool_alloc_aligned(&cmd_buffer->cmd_pool,
                                               AGX_RENDER_TARGET_LENGTH,
                                               256);

   VK_FROM_HANDLE(agxv_image_view, view, attachment->imageView);

   enum agx_channel swiz_map[] = {
      [VK_COMPONENT_SWIZZLE_R] = AGX_CHANNEL_R,
      [VK_COMPONENT_SWIZZLE_G] = AGX_CHANNEL_G,
      [VK_COMPONENT_SWIZZLE_B] = AGX_CHANNEL_B,
      [VK_COMPONENT_SWIZZLE_A] = AGX_CHANNEL_A,
      [VK_COMPONENT_SWIZZLE_ZERO] = AGX_CHANNEL_0,
      [VK_COMPONENT_SWIZZLE_ONE] = AGX_CHANNEL_1,
   };

   agx_pack(ret.cpu, RENDER_TARGET, cfg) {
      /* TODO: calculate */
      cfg.dimension = AGX_TEXTURE_DIMENSION_2D;
      cfg.layout = view->vk.image->tiling == VK_IMAGE_TILING_LINEAR ?
         AGX_LAYOUT_LINEAR : AGX_LAYOUT_COMPRESSED;

      struct agx_pixel_format_entry format =
         agx_pixel_format_from_vk(view->vk.format);

      cfg.type = format.type;
      cfg.channels = format.channels;

      #define SWIZ(c, C) \
         c == VK_COMPONENT_SWIZZLE_IDENTITY ? \
         AGX_CHANNEL_##C : swiz_map[c]

      cfg.swizzle_r = SWIZ(view->vk.swizzle.r, R);
      cfg.swizzle_g = SWIZ(view->vk.swizzle.g, G);
      cfg.swizzle_b = SWIZ(view->vk.swizzle.b, B);
      cfg.swizzle_a = SWIZ(view->vk.swizzle.a, A);

      #undef SWIZ

      cfg.width = view->image->layout.width_px;
      cfg.height = view->image->layout.height_px;
      cfg.level = 0;
      cfg.buffer = agxv_image_addr(view->image);

      if (view->vk.image->tiling == VK_IMAGE_TILING_LINEAR) {
         cfg.levels = 1;
         cfg.stride = ail_get_linear_stride_B(&view->image->layout, 0) - 0x10;
      } else {
         unreachable("hi :3 i dumb. this no implemented yet");
      }
   }
   return ret.gpu;
}

static uint32_t
agxv_build_meta(struct agxv_cmd_buffer *cmd_buffer,
                uint32_t attachment_count,
                const VkRenderingAttachmentInfo *attachments,
                bool store,
                bool partial_render)
{
   struct agx_meta_key key = {
      .tib = cmd_buffer->state.tib,
   };

   struct agx_usc_builder b =
      agx_alloc_usc_control(&cmd_buffer->shader_pool, attachment_count);

   for (uint32_t i = 0; i < attachment_count; i++) {
      if (store || partial_render) {
         switch(attachments[i].storeOp) {
         case VK_ATTACHMENT_STORE_OP_STORE:
            key.op[i] = store ? AGX_META_OP_STORE : AGX_META_OP_LOAD;
            break;
         case VK_ATTACHMENT_STORE_OP_DONT_CARE:
         case VK_ATTACHMENT_STORE_OP_NONE:
            key.op[i] = AGX_META_OP_NONE;
            break;
         default:
            unreachable("Unimplemented store op");
         }
      } else {
         switch(attachments[i].loadOp) {
         case VK_ATTACHMENT_LOAD_OP_LOAD:
            key.op[i] = AGX_META_OP_LOAD;
            break;
         case VK_ATTACHMENT_LOAD_OP_CLEAR:
            key.op[i] = AGX_META_OP_CLEAR;
            break;
         case VK_ATTACHMENT_LOAD_OP_DONT_CARE:
         case VK_ATTACHMENT_LOAD_OP_NONE_EXT:
            key.op[i] = AGX_META_OP_NONE;
            break;
         default:
            unreachable("Unimplemented load op");
         }
      }

      switch (key.op[i]) {
      case AGX_META_OP_CLEAR: {
         agx_usc_uniform(&b, 8 * i, 8,
         agx_pool_upload(&cmd_buffer->cmd_pool,
                                 &attachments[i].clearValue.color,
                                 sizeof(attachments[i].clearValue.color)));
         break;
      }
      case AGX_META_OP_STORE: {
         agx_usc_pack(&b, TEXTURE, cfg) {
            cfg.start = i;
            cfg.count = 1;
            cfg.buffer = agxv_pack_rt(cmd_buffer, &attachments[i]);
         };
         break;
      }
      default:
         break;
      }
   }

   struct agx_meta_shader *shader =
      agx_get_meta_shader(&cmd_buffer->device->meta_cache, &key);

   agx_usc_pack(&b, REGISTERS, cfg) {
      cfg.register_count = 256;
      cfg.unk_1 = true;
   }
   agx_usc_tilebuffer(&b, &cmd_buffer->state.tib);
   agx_usc_pack(&b, SHADER, cfg) {
      cfg.code = shader->ptr;
      cfg.unk_1 = 0;
      cfg.unk_2 = 0;
   }
   agx_usc_pack(&b, NO_PRESHADER, cfg);
   return agx_usc_fini(&b);
}

/* FIXME:
 * In AGXV we don't implement renderpasses for now. This means we are
 * potentially leaving performance on the table, since AGX is a tiler.
 *
 * Instead we implement dynamic rendering and common code implements
 * renderpasses for us ontop of this. There are talks of making the common
 * implementation more appropriate for tilers but that isn't in place for now
 * however this is *fine* to just get things working.
 *
 * Ideally we want some way to avoid writing to the framebuffer between each
 * subpass in common code.
 */
VKAPI_ATTR void VKAPI_CALL
agxv_CmdBeginRendering(VkCommandBuffer commandBuffer,
                       const VkRenderingInfo *pRenderingInfo)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);

   struct agxv_job *job = agxv_job_create(cmd_buffer, AGXV_JOB_GRAPHICS);

   job->render_buf.fb_width = pRenderingInfo->renderArea.extent.width;
   job->render_buf.fb_height = pRenderingInfo->renderArea.extent.height;

   /* TODO: */
   job->render_buf.utile_width = 32;
   job->render_buf.utile_height = 32;

   job->render_buf.iogpu_unk_49 = 8;
   job->render_buf.iogpu_unk_212 = 4;
   job->render_buf.iogpu_unk_214 = 0xc000;

   job->render_buf.isp_bgobjdepth = fui(0.0);
   /* TODO: don't use 1 << 9 when don't need to load? Needs more testing. */
   job->render_buf.isp_bgobjvals = 0x300;

   job->render_buf.attachment_count = pRenderingInfo->colorAttachmentCount;

   enum pipe_format tib_formats[8];

   for (uint32_t i = 0; i < pRenderingInfo->colorAttachmentCount; i++) {
      VK_FROM_HANDLE(agxv_image_view, view,
                     pRenderingInfo->pColorAttachments[i].imageView);

      job->render_buf.attachments[i].type = ASAHI_ATTACHMENT_C;
      job->render_buf.attachments[i].size = view->image->layout.size_B;
      job->render_buf.attachments[i].pointer = agxv_image_addr(view->image);

      tib_formats[i] = vk_format_to_pipe_format(view->vk.format);
   }

   cmd_buffer->state.tib = agx_build_tilebuffer_layout(tib_formats,
                                                       pRenderingInfo->colorAttachmentCount,
                                                       1);

   job->render_buf.load_pipeline =
      agxv_build_meta(cmd_buffer, pRenderingInfo->colorAttachmentCount,
                      pRenderingInfo->pColorAttachments, false, false);

   job->render_buf.store_pipeline =
      agxv_build_meta(cmd_buffer, pRenderingInfo->colorAttachmentCount,
                      pRenderingInfo->pColorAttachments, true, false);

   job->render_buf.partial_reload_pipeline =
      agxv_build_meta(cmd_buffer, pRenderingInfo->colorAttachmentCount,
                      pRenderingInfo->pColorAttachments, false, true);

   job->render_buf.partial_store_pipeline =
      agxv_build_meta(cmd_buffer, pRenderingInfo->colorAttachmentCount,
                      pRenderingInfo->pColorAttachments, true, true);

   cmd_buffer->state.job = job;

   float tan_60 = 1.732051f;
   job->render_buf.merge_upper_x = fui(tan_60 / job->render_buf.fb_width);
   job->render_buf.merge_upper_y = fui(tan_60 / job->render_buf.fb_height);
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdBindVertexBuffers2(VkCommandBuffer commandBuffer,
                           uint32_t firstBinding,
                           uint32_t bindingCount,
                           const VkBuffer *pBuffers,
                           const VkDeviceSize *pOffsets,
                           const VkDeviceSize *pSizes,
                           const VkDeviceSize *pStrides)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);

   if (pStrides) {
      vk_cmd_set_vertex_binding_strides(&cmd_buffer->vk, firstBinding,
                                        bindingCount, pStrides);
   }

   if (bindingCount) {
      cmd_buffer->state.dirty.buffers = true;
      cmd_buffer->state.dirty.vs = true;
   }

   for (uint32_t i = 0; i < bindingCount; i++) {
      VK_FROM_HANDLE(agxv_buffer, buffer, pBuffers[i]);
      cmd_buffer->state.buffers[firstBinding + i] =
         agxv_buffer_addr(buffer) + pOffsets[i];
   }
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdBindPipeline(VkCommandBuffer commandBuffer,
                     VkPipelineBindPoint pipelineBindPoint,
                     VkPipeline _pipeline)
{
   /* TODO: support compute pipelines */
   assert(pipelineBindPoint == VK_PIPELINE_BIND_POINT_GRAPHICS);
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   VK_FROM_HANDLE(agxv_graphics_pipeline, pipeline, _pipeline);

   vk_cmd_set_dynamic_graphics_state(&cmd_buffer->vk, &pipeline->dynamic);

   cmd_buffer->state.pipeline = &pipeline->base;
   cmd_buffer->state.dirty.vs = true;
   cmd_buffer->state.dirty.fs = true;
   cmd_buffer->state.dirty.pipeline = true;
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdBindDescriptorSets(VkCommandBuffer commandBuffer,
                           VkPipelineBindPoint pipelineBindPoint,
                           VkPipelineLayout _layout,
                           uint32_t firstSet,
                           uint32_t descriptorSetCount,
                           const VkDescriptorSet *pDescriptorSets,
                           uint32_t dynamicOffsetCount,
                           const uint32_t *pDynamicOffsets)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   VK_FROM_HANDLE(vk_pipeline_layout, layout, _layout);
   /* TODO: push constants len in pipeline layout */
   cmd_buffer->state.sets_base = 0;

   if (descriptorSetCount) {
      cmd_buffer->state.dirty.sets = true;

      /* FIXME: check if shader is actually needed by each stage */
      cmd_buffer->state.dirty.vs = true;
      cmd_buffer->state.dirty.fs = true;
   }

   for (uint32_t i = 0; i < descriptorSetCount; i++) {
      VK_FROM_HANDLE(agxv_descriptor_set, set, pDescriptorSets[i]);
      cmd_buffer->state.sets[firstSet + i] = agxv_descriptor_set_addr(set);
   }
}

static uint32_t
agxv_emit_vs(struct agxv_cmd_buffer *cmd_buffer)
{
   struct agx_shader_info *info =
      &cmd_buffer->state.pipeline->stages[MESA_SHADER_VERTEX].info;
   struct agx_bo *bo =
      cmd_buffer->state.pipeline->stages[MESA_SHADER_VERTEX].bo;

   struct agxv_graphics_pipeline *pipeline =
      container_of(cmd_buffer->state.pipeline, struct agxv_graphics_pipeline,
                   base);

   struct agx_usc_builder b =
      agx_alloc_usc_control(&cmd_buffer->shader_pool,
                            3);
   agx_usc_shared_none(&b);

   /* FIXME: these are only needed when buffers or sets are dirty. */
   uint64_t pushs = agx_pool_upload_aligned(&cmd_buffer->cmd_pool,
                                            cmd_buffer->state.push,
                                            sizeof(cmd_buffer->state.push), 64);

   uint64_t buffers = agx_pool_upload_aligned(&cmd_buffer->cmd_pool,
                                              cmd_buffer->state.buffers,
                                              sizeof(cmd_buffer->state.buffers), 64);

   uint64_t sets = agx_pool_upload_aligned(&cmd_buffer->cmd_pool,
                                           cmd_buffer->state.sets,
                                           sizeof(cmd_buffer->state.sets), 64);

   uint32_t push_base = cmd_buffer->state.push_base;
   uint32_t sets_base = cmd_buffer->state.sets_base;
   uint32_t push_len = sets_base - push_base;

   /* We must get the buffer base from the pipeline not calculate it here to
    * maintain pipeline layout compatibility
    */
   uint32_t sets_len = pipeline->buffer_base - sets_base;

   if (push_len)
      agx_usc_uniform(&b, push_base, push_len, pushs);

   if (sets_len)
      agx_usc_uniform(&b, sets_base, sets_len, sets);

   if (pipeline->buffer_len)
      agx_usc_uniform(&b, pipeline->buffer_base, pipeline->buffer_len, buffers);

   agx_usc_pack(&b, SHADER, cfg) {
      cfg.code = bo->ptr.gpu;
      cfg.unk_1 = 0;
      cfg.unk_2 = 2;
   }
   agx_usc_pack(&b, REGISTERS, cfg) {
      cfg.register_count = 256;
   }

   if (info->has_preamble) {
      agx_usc_pack(&b, PRESHADER, cfg) {
         cfg.code = bo->ptr.gpu + info->preamble_offset;
      }
   } else {
      agx_usc_pack(&b, NO_PRESHADER, cfg);
   }
   return agx_usc_fini(&b);
}

static uint32_t
agxv_emit_fs(struct agxv_cmd_buffer *cmd_buffer)
{
   struct agx_shader_info *info =
      &cmd_buffer->state.pipeline->stages[MESA_SHADER_FRAGMENT].info;
   struct agx_bo *bo =
      cmd_buffer->state.pipeline->stages[MESA_SHADER_FRAGMENT].bo;

   struct agxv_graphics_pipeline *pipeline =
      container_of(cmd_buffer->state.pipeline, struct agxv_graphics_pipeline,
                   base);

   struct agx_usc_builder b =
      agx_alloc_usc_control(&cmd_buffer->shader_pool, 3);

   agx_usc_tilebuffer(&b, &cmd_buffer->state.tib);

   /* FIXME: these are only needed when sets are dirty */
   uint64_t pushs = agx_pool_upload_aligned(&cmd_buffer->cmd_pool,
                                            cmd_buffer->state.push,
                                            sizeof(cmd_buffer->state.push), 64);

   uint64_t sets = agx_pool_upload_aligned(&cmd_buffer->cmd_pool,
                                           cmd_buffer->state.sets,
                                           sizeof(cmd_buffer->state.sets), 64);

   uint32_t push_base = cmd_buffer->state.push_base;
   uint32_t sets_base = cmd_buffer->state.sets_base;
   uint32_t push_len = sets_base - push_base;

   uint32_t sets_len = pipeline->buffer_base - sets_base;

   if (push_len)
      agx_usc_uniform(&b, push_base, push_len, pushs);

   if (sets_len)
      agx_usc_uniform(&b, sets_base, sets_len, sets);

   agx_usc_pack(&b, SHADER, cfg) {
      cfg.code = bo->ptr.gpu;
      cfg.unk_1 = false;
      cfg.unk_2 = 1;
      cfg.loads_varyings = true;
   }
   agx_usc_pack(&b, REGISTERS, cfg) {
      cfg.register_count = 256;
   }
   agx_usc_pack(&b, FRAGMENT_PROPERTIES, cfg) {

   }

   if (info->has_preamble) {
      agx_usc_pack(&b, PRESHADER, cfg) {
         cfg.code = bo->ptr.gpu + info->preamble_offset;
      }
   } else {
      agx_usc_pack(&b, NO_PRESHADER, cfg);
   }
   return agx_usc_fini(&b);
}

static void
agxv_emit_ppp(struct agxv_cmd_buffer *cmd_buffer)
{
   uint8_t *buf = cmd_buffer->state.encoder_current;

   struct agxv_graphics_pipeline *pipeline =
      container_of(cmd_buffer->state.pipeline, struct agxv_graphics_pipeline,
                   base);

   struct vk_dynamic_graphics_state *state =
      &cmd_buffer->vk.dynamic_graphics_state;
   BITSET_WORD *dirty = state->dirty;

#define IS_DIRTY(ST) BITSET_TEST(dirty, MESA_VK_DYNAMIC_##ST)

   struct AGX_PPP_HEADER ppp_header = {
      .fragment_control =
         IS_DIRTY(DS_DEPTH_TEST_ENABLE) ||
         IS_DIRTY(DS_STENCIL_TEST_ENABLE) ||
         IS_DIRTY(RS_DEPTH_BIAS_ENABLE),

      .fragment_control_2 = cmd_buffer->state.dirty.pipeline ||
         IS_DIRTY(IA_PRIMITIVE_TOPOLOGY),

      .fragment_front_face = cmd_buffer->state.dirty.pipeline ||
         IS_DIRTY(DS_STENCIL_REFERENCE) ||
         IS_DIRTY(RS_LINE_WIDTH),

      .fragment_front_face_2 = IS_DIRTY(IA_PRIMITIVE_TOPOLOGY),

      /* TODO: */
      .fragment_front_stencil = false,

      .fragment_back_face = cmd_buffer->state.dirty.pipeline ||
         IS_DIRTY(DS_STENCIL_REFERENCE) ||
         IS_DIRTY(RS_LINE_WIDTH),

      .fragment_back_face_2 = IS_DIRTY(IA_PRIMITIVE_TOPOLOGY),

      /* TODO: */
      .fragment_back_stencil = false,

      /* TODO:
       *
       * Depth bias and scissors are stored a seperate bos;
       * only indexes are packed here.
       */
      .depth_bias_scissor = false,

      /* TODO: this isn't correct; this should be updated less freqently than
       * on pipeline bind
       */
      .region_clip = cmd_buffer->state.dirty.pipeline,

      .viewport = IS_DIRTY(VP_VIEWPORTS),

      /* TODO: this isn't correct; this should be updated less freqently than
       * on pipeline bind
       */
      .w_clamp = cmd_buffer->state.dirty.pipeline,

      .output_select = cmd_buffer->state.dirty.pipeline,

      .varying_word_0 = cmd_buffer->state.dirty.pipeline,
      .varying_word_1 = cmd_buffer->state.dirty.pipeline,

      .cull = IS_DIRTY(RS_CULL_MODE),

      /* TODO: this isn't correct; this should be updated less freqently than
       * on pipeline bind
       */
      .cull_2 = cmd_buffer->state.dirty.pipeline,
      .fragment_shader = cmd_buffer->state.dirty.fs,
      .occlusion_query = false,
      .occlusion_query_2 = false,
      .output_unknown = cmd_buffer->state.dirty.pipeline,
      .output_size = cmd_buffer->state.dirty.pipeline,
      .varying_word_2 = cmd_buffer->state.dirty.pipeline,
   };

#undef IS_DIRTY

   struct agx_ppp_update ppp = agx_new_ppp_update(&cmd_buffer->cmd_pool,
                                                  ppp_header);

#define ppp_push_if_dirty(name, NAME, cfg) \
   if (ppp_header.name) \
      agx_ppp_push(&ppp, NAME, cfg)

   uint8_t *start_head = ppp.head;

   ppp_push_if_dirty(fragment_control, FRAGMENT_CONTROL, cfg) {
      cfg.stencil_test_enable = state->ds.stencil.test_enable;
      cfg.two_sided_stencil = state->ds.stencil.test_enable;
      cfg.depth_bias_enable = state->rs.depth_bias.enable;

      /* TODO: points */
      cfg.disable_tri_merging = false;
      cfg.scissor_enable = false;
      //cfg.visibility_mode = AGX_VISIBILITY_MODE_NONE;
   }
   ppp_push_if_dirty(fragment_control_2, FRAGMENT_CONTROL, cfg) {
      switch (state->ia.primitive_topology) {
      case VK_PRIMITIVE_TOPOLOGY_POINT_LIST:
      case VK_PRIMITIVE_TOPOLOGY_LINE_LIST:
      case VK_PRIMITIVE_TOPOLOGY_LINE_STRIP:
         cfg.disable_tri_merging = true;
         break;
      default:
         cfg.disable_tri_merging = false;
      }
      cfg.disable_tri_merging = false;

      /* TODO: read shader metadata */
      cfg.no_colour_output = false &&
         cmd_buffer->state.pipeline->stages[MESA_SHADER_FRAGMENT].info.no_colour_output;
      cfg.pass_type = AGX_PASS_TYPE_OPAQUE;
   }
   ppp_push_if_dirty(fragment_front_face, FRAGMENT_FACE, cfg) {
      cfg.disable_depth_write = true;
      cfg.depth_function = AGX_ZS_FUNC_ALWAYS;

      cfg.stencil_reference = state->ds.stencil.front.reference;
      cfg.line_width = state->rs.line.width;
      cfg.polygon_mode = AGX_POLYGON_MODE_FILL;
   }
   ppp_push_if_dirty(fragment_front_face_2, FRAGMENT_FACE_2, cfg)
      cfg.object_type = AGX_OBJECT_TYPE_TRIANGLE;
   ppp_push_if_dirty(fragment_front_stencil, FRAGMENT_STENCIL, cfg);
   ppp_push_if_dirty(fragment_back_face, FRAGMENT_FACE, cfg) {
      cfg.disable_depth_write = true;
      cfg.depth_function = AGX_ZS_FUNC_ALWAYS;

      cfg.stencil_reference = state->ds.stencil.front.reference;
      cfg.line_width = state->rs.line.width;
      cfg.polygon_mode = AGX_POLYGON_MODE_FILL;
   }
   ppp_push_if_dirty(fragment_back_face_2, FRAGMENT_FACE_2, cfg)
      cfg.object_type = AGX_OBJECT_TYPE_TRIANGLE;
   ppp_push_if_dirty(fragment_back_stencil, FRAGMENT_STENCIL, cfg);
   ppp_push_if_dirty(depth_bias_scissor, DEPTH_BIAS_SCISSOR, cfg);
   ppp_push_if_dirty(region_clip, REGION_CLIP, cfg) {
      cfg.max_x = 8;
      cfg.max_y = 8;
      cfg.enable = false;
   }
   ppp_push_if_dirty(viewport, VIEWPORT, cfg) {
      /* FIXME: we only support one viewport for now */
      float x = state->vp.viewports[0].x;
      float y = state->vp.viewports[0].y;

      float half_width = 0.5f * state->vp.viewports[0].width;
      float half_height = 0.5f * state->vp.viewports[0].height;

      float n = state->vp.viewports[0].minDepth;
      float f = state->vp.viewports[0].maxDepth;

      cfg.translate_x = half_width + x;
      cfg.translate_y = half_height + y;
      cfg.translate_z = n;
      cfg.scale_x = half_width;
      cfg.scale_y = half_height;
      cfg.scale_z = f - n;
   }
   ppp_push_if_dirty(w_clamp, W_CLAMP, cfg)
      cfg.w_clamp = 0.000001f;
   ppp_push_if_dirty(output_select, OUTPUT_SELECT, cfg) {
      cfg.varyings = 1;
      cfg.point_size = false;
      cfg.frag_coord_z = false;
   }
   ppp_push_if_dirty(varying_word_0, VARYING_0, cfg) cfg.count = 4;
   ppp_push_if_dirty(varying_word_1, VARYING_1, cfg);
   ppp_push_if_dirty(cull, CULL, cfg) {
      cfg.cull_front = state->rs.cull_mode & VK_CULL_MODE_FRONT_BIT;
      cfg.cull_back = state->rs.cull_mode & VK_CULL_MODE_BACK_BIT;
      cfg.depth_clip = true;
      cfg.depth_clamp = false;
      cfg.front_face_ccw =
         state->rs.front_face == VK_FRONT_FACE_COUNTER_CLOCKWISE;
   }
   ppp_push_if_dirty(cull_2, CULL_2, cfg)
      cfg.unknown_2 = 0xa0;
   ppp_push_if_dirty(fragment_shader, FRAGMENT_SHADER, cfg) {
      cfg.pipeline = agxv_emit_fs(cmd_buffer);
      cfg.uniform_register_count =
         cmd_buffer->state.pipeline->stages[MESA_SHADER_FRAGMENT].info.push_count;
      cfg.cf_binding_count =
         pipeline->base.stages[MESA_SHADER_FRAGMENT].info.varyings.fs.nr_bindings;
      if (cfg.cf_binding_count)
         cfg.cf_bindings = pipeline->cf_bindings->ptr.gpu;
   };
   ppp_push_if_dirty(occlusion_query, FRAGMENT_OCCLUSION_QUERY, cfg);
   ppp_push_if_dirty(occlusion_query_2, FRAGMENT_OCCLUSION_QUERY_2, cfg);
   ppp_push_if_dirty(output_unknown, OUTPUT_UNKNOWN, cfg);
   ppp_push_if_dirty(output_size, OUTPUT_SIZE, cfg) cfg.count = 8;
   ppp_push_if_dirty(varying_word_2, VARYING_2, cfg);

#undef ppp_push_if_dirty
   if (ppp.head > start_head)
      agx_ppp_fini(&buf, &ppp);

   cmd_buffer->state.encoder_current = buf;
}

static void
agxv_emit(struct agxv_cmd_buffer *cmd_buffer)
{
   if (cmd_buffer->state.dirty.vs) {
      uint8_t *buf = cmd_buffer->state.encoder_current;

      agxv_push(buf, VDM_STATE, cfg) {
         cfg.vertex_shader_word_0_present = true;
         cfg.vertex_shader_word_1_present = true;
         cfg.vertex_outputs_present = true;
         cfg.vertex_unknown_present = true;
      }

      agxv_push(buf, VDM_STATE_VERTEX_SHADER_WORD_0, cfg) {
         cfg.uniform_register_count =
            cmd_buffer->state.pipeline->stages[MESA_SHADER_VERTEX].info.push_count;
      };

      agxv_push(buf, VDM_STATE_VERTEX_SHADER_WORD_1, cfg) {
         cfg.pipeline = agxv_emit_vs(cmd_buffer); /* TODO */
      }

      agxv_push(buf, VDM_STATE_VERTEX_OUTPUTS, cfg) {
         cfg.output_count_1 = 8;
         cfg.output_count_2 = 8;
      }

      agxv_push(buf, VDM_STATE_VERTEX_UNKNOWN, cfg) {
      }

      cmd_buffer->state.encoder_current = buf;

      memset(cmd_buffer->state.encoder_current, 0, 4);
      cmd_buffer->state.encoder_current += 4;
   }


   agxv_emit_ppp(cmd_buffer);

   cmd_buffer->state.dirty.vs = false;
   cmd_buffer->state.dirty.fs = false;
   cmd_buffer->state.dirty.pipeline = false;
   cmd_buffer->state.dirty.sets = false;
   cmd_buffer->state.dirty.buffers = false;
   cmd_buffer->state.dirty.push = false;
   vk_dynamic_graphics_state_clear(&cmd_buffer->vk.dynamic_graphics_state);
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdDraw(VkCommandBuffer commandBuffer,
             uint32_t vertexCount,
             uint32_t instanceCount,
             uint32_t firstVertex,
             uint32_t firstInstance)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);

   if (!vertexCount)
      return;

   if (!instanceCount)
      return;

   /* FIXME: handle cmd_buffer resizing */
   agxv_emit(cmd_buffer);

   uint8_t *buf = cmd_buffer->state.encoder_current;

   agxv_push(buf, INDEX_LIST, cfg) {
      cfg.index_buffer_hi = 0x15;
      cfg.primitive = AGX_PRIMITIVE_TRIANGLE_STRIP;
   }

   agxv_push(buf, INDEX_LIST_COUNT, cfg) {
      cfg.count = vertexCount;
   }

   agxv_push(buf, INDEX_LIST_INSTANCES, cfg) {
      cfg.count = instanceCount;
   }

   agxv_push(buf, INDEX_LIST_START, cfg) {
      cfg.start = firstVertex;
   }

   cmd_buffer->state.encoder_current = buf;
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdEndRendering(VkCommandBuffer commandBuffer)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   uint8_t *buf = cmd_buffer->state.encoder_current;
   agxv_push(buf, VDM_STREAM_TERMINATE, cfg);

   agxdecode_drm_cmd_render(&cmd_buffer->state.job->render_buf, true);
   agxdecode_next_frame();
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdPipelineBarrier2(VkCommandBuffer commandBuffer,
                         const VkDependencyInfo *pDependencyInfo)
{
   /* TODO: figure out what to do here */
}

/* TODO: this should probably go somewhere else with all the other transfer commands */
VKAPI_ATTR void VKAPI_CALL
agxv_CmdCopyBuffer2(VkCommandBuffer commandBuffer,
                    const VkCopyBufferInfo2 *pCopyBufferInfo)
{
   /* TODO: do this ahead of time */
   /* TODO: faster block sizes than just one byte at a time */
   struct util_dynarray bin;
   util_dynarray_init(&bin, NULL);
   {
      nir_builder b = nir_builder_init_simple_shader(MESA_SHADER_COMPUTE,
                                                      &agx_nir_options, "agxv_cmd_copy_buffer");
      nir_ssa_def *offset = nir_channel(&b, nir_load_global_invocation_id(&b, 32), 0);
      nir_ssa_def *src_buf = nir_load_preamble(&b, 1, 64, .base = 0);
      nir_ssa_def *dst_buf = nir_load_preamble(&b, 1, 64, .base = 4);
      nir_ssa_def *value = nir_load_agx(&b, 1, 8, src_buf, offset, .format = PIPE_FORMAT_R8_UINT);
      nir_store_agx(&b, value, dst_buf, offset, .format = PIPE_FORMAT_R8_UINT);

      struct agx_shader_key key = {
         .reserved_preamble = 8
      };
      struct agx_shader_info info;
      agx_preprocess_nir(b.shader, false);
      agx_compile_shader_nir(b.shader, &key, NULL, &bin, &info);
   }

   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   VK_FROM_HANDLE(agxv_buffer, src, pCopyBufferInfo->srcBuffer);
   VK_FROM_HANDLE(agxv_buffer, dst, pCopyBufferInfo->dstBuffer);
   struct agxv_job *job = agxv_job_create(cmd_buffer, AGXV_JOB_COMPUTE);
   uint8_t *buf = cmd_buffer->state.encoder_current;

   for (uint32_t i = 0; i < pCopyBufferInfo->regionCount; i++) {
      const VkBufferCopy2 *region = &pCopyBufferInfo->pRegions[i];

      struct agx_usc_builder b =
         agx_alloc_usc_control(&cmd_buffer->shader_pool, 1);

      uint64_t ptrs[] = {
         agxv_buffer_addr(src) + region->srcOffset,
         agxv_buffer_addr(dst) + region->dstOffset,
      };
      uint64_t ptr = agx_pool_upload(&cmd_buffer->cmd_pool,
                                     ptrs,
                                     sizeof(ptrs));
      agx_usc_uniform(&b, 0, 8, ptr);

      agx_usc_shared_none(&b);
      ptr = agx_pool_upload(&cmd_buffer->shader_pool,
                            bin.data,
                            bin.size);
      agx_usc_pack(&b, SHADER, cfg) {
         cfg.code = ptr;
         cfg.unk_1 = 0;
         cfg.unk_2 = 1;
      }

      agx_usc_pack(&b, REGISTERS, cfg) {
         cfg.register_count = 16;
         cfg.unk_1 = false;
      }

      agx_usc_pack(&b, NO_PRESHADER, cfg);

      uint32_t pipeline = agx_usc_fini(&b);

      agxv_push(buf, LAUNCH, cfg) {
         cfg.uniform_register_count = 64;
         cfg.texture_state_register_count = 256;
         cfg.preshader_register_count = 16;
         cfg.pipeline = pipeline; /* TODO: */
         cfg.group_count_x = region->size;
         cfg.group_count_y = 1;
         cfg.group_count_z = 1;
         cfg.local_size_x = 1;
         cfg.local_size_y = 1;
         cfg.local_size_z = 1;
      };
   }
   agxv_push(buf, CDM_STREAM_TERMINATE, cfg);
   cmd_buffer->state.job = job;
   job->compute_buf.encoder_end = (uintptr_t)buf;

   agxdecode_drm_cmd_compute(&cmd_buffer->state.job->compute_buf, true);
   agxdecode_next_frame();
}

static void
agxv_copy_image(struct agxv_cmd_buffer *cmd_buffer,
                struct agxv_image *src, VkOffset3D src_offset, VkImageSubresourceLayers src_res,
                struct agxv_image *dst, VkOffset3D dst_offset, VkImageSubresourceLayers dst_res,
                VkExtent3D extent)
{
   uint8_t *buf = cmd_buffer->state.encoder_current;
   /* TODO: do this ahead of time */
   /* TODO: faster block sizes than just one byte at a time */
   struct util_dynarray bin;
   util_dynarray_init(&bin, NULL);
   {
      nir_builder b = nir_builder_init_simple_shader(MESA_SHADER_COMPUTE,
                                                      &agx_nir_options, "agxv_cmd_copy_buffer");
      nir_ssa_def *offset = nir_channel(&b, nir_load_global_invocation_id(&b, 32), 0);
      nir_ssa_def *src_buf = nir_load_preamble(&b, 1, 64, .base = 0);
      nir_ssa_def *dst_buf = nir_load_preamble(&b, 1, 64, .base = 4);
      nir_ssa_def *value = nir_load_agx(&b, 1, 8, src_buf, offset, .format = PIPE_FORMAT_R8_UINT);
      nir_store_agx(&b, value, dst_buf, offset, .format = PIPE_FORMAT_R8_UINT);
      nir_block_image_store_agx(&b, nir_imm_int(&b, 0),
                                nir_imm_intN_t(&b, 0, 16), .format = PIPE_FORMAT_R8_UINT,
                                .image_dim = GLSL_SAMPLER_DIM_2D);

      struct agx_shader_key key = {
         .reserved_preamble = 8
      };
      struct agx_shader_info info;
      agx_preprocess_nir(b.shader, false);
      agx_compile_shader_nir(b.shader, &key, NULL, &bin, &info);
   }

   struct agx_usc_builder b =
      agx_alloc_usc_control(&cmd_buffer->shader_pool, 3);

   uint64_t ptrs[] = {
      agxv_image_addr(src),
      agxv_image_addr(dst), // TODO: image offset
   };
   uint64_t ptr = agx_pool_upload(&cmd_buffer->cmd_pool,
                                  ptrs,
                                  sizeof(ptrs));
   agx_usc_uniform(&b, 0, 8, ptr);



   agx_usc_shared_none(&b);
   ptr = agx_pool_upload(&cmd_buffer->shader_pool,
                         bin.data,
                         bin.size);
   agx_usc_pack(&b, SHADER, cfg) {
      cfg.code = ptr;
      cfg.unk_1 = 0;
      cfg.unk_2 = 1;
   }

   agx_usc_pack(&b, REGISTERS, cfg) {
      cfg.register_count = 16;
      cfg.unk_1 = false;
   }

   agx_usc_pack(&b, NO_PRESHADER, cfg);

   uint32_t pipeline = agx_usc_fini(&b);

   agxv_push(buf, LAUNCH, cfg) {
      cfg.uniform_register_count = 64;
      cfg.texture_state_register_count = 256;
      cfg.preshader_register_count = 16;
      cfg.pipeline = pipeline; /* TODO: */
      cfg.group_count_x = extent.width;
      cfg.group_count_y = extent.height;
      cfg.group_count_z = 1;
      cfg.local_size_x = 1;
      cfg.local_size_y = 1;
      cfg.local_size_z = 1;
   };

   cmd_buffer->state.encoder_current = buf;
}

static struct agxv_image *
agxv_create_buffer_image_alias(struct agxv_device *device,
                               struct agxv_buffer *buffer, VkDeviceSize offset,
                               uint32_t row_length, uint32_t height)
{
   VkImage _image;
   agxv_CreateImage(
      agxv_device_to_handle(device),
       &(VkImageCreateInfo){
      .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
      /* TODO: don't hard code */
      .imageType = VK_IMAGE_TYPE_2D,
      .format = VK_FORMAT_R8G8B8A8_UNORM,
      .extent.width = row_length,
      .extent.height = height,
      .extent.depth = 1,
      .mipLevels = 1,
      .arrayLayers = 1,
      .samples = VK_SAMPLE_COUNT_1_BIT,
      .usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT
   }, NULL, &_image);
   VK_FROM_HANDLE(agxv_image, image, _image);

   image->mem = buffer->mem;
   image->offset = offset;

   return image;
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdCopyBufferToImage2(VkCommandBuffer commandBuffer,
                    const VkCopyBufferToImageInfo2 *pCopyBufferInfo)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   VK_FROM_HANDLE(agxv_buffer, src, pCopyBufferInfo->srcBuffer);
   VK_FROM_HANDLE(agxv_image, dst, pCopyBufferInfo->dstImage);
   struct agxv_job *job = agxv_job_create(cmd_buffer, AGXV_JOB_COMPUTE);

   for (uint32_t i = 0; i < pCopyBufferInfo->regionCount; i++) {
      const VkBufferImageCopy2 *region = &pCopyBufferInfo->pRegions[i];
      // TODO: cleanup alias
      struct agxv_image *alias =
         agxv_create_buffer_image_alias(cmd_buffer->device,
                                        src,
                                        region->bufferOffset,
                                        region->bufferRowLength ? region->bufferRowLength : region->imageExtent.width,
                                        region->bufferImageHeight ? region->bufferImageHeight : region->imageExtent.height);
      agxv_copy_image(cmd_buffer, alias, (VkOffset3D){}, (VkImageSubresourceLayers){
         .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT, .layerCount = 1
      }, dst, region->imageOffset, region->imageSubresource, region->imageExtent);
   }
   uint8_t *buf = cmd_buffer->state.encoder_current;
   agxv_push(buf, CDM_STREAM_TERMINATE, cfg);
   cmd_buffer->state.job = job;
   job->compute_buf.encoder_end = (uintptr_t)buf;

   agxdecode_drm_cmd_compute(&cmd_buffer->state.job->compute_buf, true);
   agxdecode_next_frame();
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdCopyImageToBuffer2(VkCommandBuffer commandBuffer,
                    const VkCopyImageToBufferInfo2 *pCopyBufferInfo)
{
   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   VK_FROM_HANDLE(agxv_image, src, pCopyBufferInfo->srcImage);
   VK_FROM_HANDLE(agxv_buffer, dst, pCopyBufferInfo->dstBuffer);
   struct agxv_job *job = agxv_job_create(cmd_buffer, AGXV_JOB_COMPUTE);

   for (uint32_t i = 0; i < pCopyBufferInfo->regionCount; i++) {
      const VkBufferImageCopy2 *region = &pCopyBufferInfo->pRegions[i];
      // TODO: cleanup alias
      struct agxv_image *alias =
         agxv_create_buffer_image_alias(cmd_buffer->device,
                                        dst,
                                        region->bufferOffset,
                                        region->bufferRowLength ? region->bufferRowLength : region->imageExtent.width,
                                        region->bufferImageHeight ? region->bufferImageHeight : region->imageExtent.height);
      agxv_copy_image(cmd_buffer, src, region->imageOffset, region->imageSubresource,
                      alias, (VkOffset3D){}, (VkImageSubresourceLayers){
         .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT, .layerCount = 1
      }, region->imageExtent);
   }
   uint8_t *buf = cmd_buffer->state.encoder_current;
   agxv_push(buf, CDM_STREAM_TERMINATE, cfg);
   cmd_buffer->state.job = job;
   job->compute_buf.encoder_end = (uintptr_t)buf;

   agxdecode_drm_cmd_compute(&cmd_buffer->state.job->compute_buf, true);
   agxdecode_next_frame();
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdFillBuffer(VkCommandBuffer commandBuffer,
                   VkBuffer dstBuffer,
                   VkDeviceSize dstOffset,
                   VkDeviceSize size,
                   uint32_t data)
{
   /* TODO: do this ahead of time */
   struct util_dynarray bin;
   util_dynarray_init(&bin, NULL);
   {
      nir_builder b = nir_builder_init_simple_shader(MESA_SHADER_COMPUTE,
                                                      &agx_nir_options, "agxv_cmd_fill_buffer");
      nir_ssa_def *value = nir_load_preamble(&b, 1, 32, .base = 0);
      nir_ssa_def *dst_buf = nir_load_preamble(&b, 1, 64, .base = 4);
      nir_ssa_def *offset = nir_channel(&b, nir_load_global_invocation_id(&b, 32), 0);
      dst_buf = nir_iadd(&b, dst_buf, nir_u2u64(&b, offset));
      nir_store_global(&b, dst_buf, sizeof(uint32_t), value, 1);

      struct agx_shader_key key = {
         .reserved_preamble = 8
      };
      struct agx_shader_info info;
      agx_preprocess_nir(b.shader, false);
      agx_compile_shader_nir(b.shader, &key, NULL, &bin, &info);
   }

   VK_FROM_HANDLE(agxv_cmd_buffer, cmd_buffer, commandBuffer);
   VK_FROM_HANDLE(agxv_buffer, dst, dstBuffer);
   struct agxv_job *job = agxv_job_create(cmd_buffer, AGXV_JOB_COMPUTE);
   uint8_t *buf = cmd_buffer->state.encoder_current;

   struct agx_usc_builder b =
      agx_alloc_usc_control(&cmd_buffer->shader_pool, 1);

   uint64_t ptrs[2] = {
      data,
      agxv_buffer_addr(dst)
   };
   uint64_t ptr = agx_pool_upload(&cmd_buffer->cmd_pool,
                                  ptrs,
                                  sizeof(ptrs));
   agx_usc_uniform(&b, 0, 8, ptr);

   agx_usc_shared_none(&b);
   ptr = agx_pool_upload(&cmd_buffer->shader_pool,
                         bin.data,
                         bin.size);
   agx_usc_pack(&b, SHADER, cfg) {
      cfg.code = ptr;
      cfg.unk_1 = 0;
      cfg.unk_2 = 1;
   }

   agx_usc_pack(&b, REGISTERS, cfg) {
      cfg.register_count = 16;
      cfg.unk_1 = false;
   }

   agx_usc_pack(&b, NO_PRESHADER, cfg);

   uint32_t pipeline = agx_usc_fini(&b);

   agxv_push(buf, LAUNCH, cfg) {
      cfg.uniform_register_count = 64;
      cfg.texture_state_register_count = 256;
      cfg.preshader_register_count = 16;
      cfg.pipeline = pipeline; /* TODO: */
      cfg.group_count_x = 32;
      cfg.group_count_y = 1;
      cfg.group_count_z = 1;
      cfg.local_size_x = 32;
      cfg.local_size_y = 1;
      cfg.local_size_z = 1;
   };
   agxv_push(buf, CDM_STREAM_TERMINATE, cfg);
   cmd_buffer->state.job = job;
   job->compute_buf.encoder_end = (uintptr_t)buf;

   agxdecode_drm_cmd_compute(&cmd_buffer->state.job->compute_buf, true);
   agxdecode_next_frame();
}

VKAPI_ATTR void VKAPI_CALL
agxv_CmdClearColorImage(VkCommandBuffer commandBuffer,
                        VkImage image,
                        VkImageLayout imageLayout,
                        const VkClearColorValue *pColor,
                        uint32_t rangeCount,
                        const VkImageSubresourceRange *pRanges)
{

}
