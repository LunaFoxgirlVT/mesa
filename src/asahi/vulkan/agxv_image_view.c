#include "agxv_image_view.h"

#include "agxv_image.h"

#include "agxv_device.h"

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateImageView(VkDevice _device,
                     const VkImageViewCreateInfo *pCreateInfo,
                     const VkAllocationCallbacks *pAllocator,
                     VkImageView *pView)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_image, image, pCreateInfo->image);
   /* TODO: do we even need an agxv_image_view if we can't prepack the
    * descriptor? */
   struct agxv_image_view *view;

   view = vk_image_view_create(&device->vk, false,
                               pCreateInfo, pAllocator, sizeof(*view));

   if (!view)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   view->image = image;

   *pView = vk_image_view_to_handle(&view->vk);

   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_DestroyImageView(VkDevice _device,
                      VkImageView imageView,
                      const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_image_view, view, imageView);

   if (!view)
      return;

   vk_free2(&device->vk.alloc, pAllocator, view);
}
