#ifndef AGXV_COMPUTE_PIPELINE
#define AGXV_COMPUTE_PIPELINE 1

#include "agxv_pipeline.h"

struct agxv_compute_pipeline {
   struct agxv_pipeline base;
};

VK_DEFINE_NONDISP_HANDLE_CASTS(agxv_compute_pipeline, base.base, VkPipeline,
                               VK_OBJECT_TYPE_PIPELINE);

#endif
