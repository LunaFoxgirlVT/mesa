#ifndef AGXV_CMD_BUFFER
#define AGXV_CMD_BUFFER 1

#include "agxv_private.h"

#include "asahi/lib/pool.h"
#include "drm-uapi/asahi_drm.h"
#include "asahi/lib/agx_tilebuffer.h"

#include "vulkan/runtime/vk_command_buffer.h"

enum agxv_job_type {
   AGXV_JOB_GRAPHICS,
   AGXV_JOB_COMPUTE
};

#define AGXV_MAX_BUFFERS 32
#define AGXV_MAX_PUSH 16
#define AGXV_MAX_SETS 32

struct agxv_job {
   struct list_head link;
   enum agxv_job_type type;
   union {
      struct drm_asahi_cmd_compute compute_buf;
      struct drm_asahi_cmd_render render_buf;
   };
};

struct agxv_state {
   /* current job that's being assembled */
   struct agxv_job *job;

   struct agx_ptr encoder;
   /* current encoder pointer */
   uint8_t *encoder_current;

   uint8_t push[AGXV_MAX_PUSH];
   struct agx_bo *push_bo;
   uint32_t push_base;

   uint64_t sets[AGXV_MAX_SETS];
   struct agx_bo *sets_bo;
   uint32_t sets_base;

   uint64_t buffers[AGXV_MAX_BUFFERS];
   struct agx_bo *buffers_bo;

   struct agx_tilebuffer_layout tib;
   struct agxv_pipeline *pipeline;

   struct {
      /* If vertex buffers have been updated, on VDM state emission we need to
       * upload buffers to a new bo and replace the buffers_bo in the job with
       * the new bo. Otherwise we use the old bo from the job initialised on
       * job creation.
       */
      bool buffers : 1;
      bool sets : 1;
      bool push : 1;

      /* If shader state needs updating */
      bool vs : 1;
      bool fs : 1;

      bool pipeline : 1;
   } dirty;
};

struct agxv_cmd_buffer {
   struct vk_command_buffer vk;

   struct agxv_device *device;

   /* allocates command buffer memory */
   struct agx_pool cmd_pool;
   /* allocates memory for load/store shaders */
   struct agx_pool shader_pool;
   /* allocates memory < 4GB */
   struct agx_pool pipeline_pool;

   struct agxv_state state;

   struct list_head jobs;
};

VK_DEFINE_HANDLE_CASTS(agxv_cmd_buffer, vk.base, VkCommandBuffer,
                       VK_OBJECT_TYPE_COMMAND_BUFFER)

#endif
