#include "agxv_image.h"

#include "agxv_device.h"
#include "agxv_device_memory.h"

#include "vk_format.h"

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateImage(VkDevice _device,
                 const VkImageCreateInfo *pCreateInfo,
                 const VkAllocationCallbacks *pAllocator,
                 VkImage *pImage)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   struct agxv_image *image;

   image = vk_image_create(&device->vk, pCreateInfo,
                           pAllocator, sizeof(*image));

   if (!image)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   image->layout.sample_count_sa = 1;
   image->layout.width_px = image->vk.extent.width;
   image->layout.height_px = image->vk.extent.height;
   image->layout.depth_px = image->vk.extent.depth;
   image->layout.format = vk_format_to_pipe_format(image->vk.format);
   // image->layout.levels = image->vk.mip_levels;
   image->layout.levels = 1;
   image->layout.tiling = image->vk.tiling == VK_IMAGE_TILING_OPTIMAL ?
      AIL_TILING_TWIDDLED : AIL_TILING_LINEAR;

   ail_make_miptree(&image->layout);

   *pImage = agxv_image_to_handle(image);

   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_GetImageMemoryRequirements2(VkDevice _device,
                                 const VkImageMemoryRequirementsInfo2 *pInfo,
                                 VkMemoryRequirements2 *pMemoryRequirements)
{
   VK_FROM_HANDLE(agxv_image, image, pInfo->image);
   pMemoryRequirements->memoryRequirements = (VkMemoryRequirements) {
      .size = image->layout.size_B,
      .memoryTypeBits = 1,
      .alignment = 0x10000, /* FIXME: using page size for now */
   };

   vk_foreach_struct_const(ext, pInfo->pNext) {
      agxv_debug_ignored_stype(ext->sType);
   }
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_BindImageMemory2(VkDevice _device,
                      uint32_t bindInfoCount,
                      const VkBindImageMemoryInfo *pBindInfos)
{
   for (uint32_t i = 0; i < bindInfoCount; i++) {
      VK_FROM_HANDLE(agxv_image, image, pBindInfos->image);
      VK_FROM_HANDLE(agxv_device_memory, mem, pBindInfos->memory);

      image->mem = mem;
      image->offset = pBindInfos->memoryOffset;
   }
   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_DestroyImage(VkDevice _device,
                  VkImage _image,
                  const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_image, image, _image);

   if (!image)
      return;

   vk_free2(&device->vk.alloc, pAllocator, image);
}
