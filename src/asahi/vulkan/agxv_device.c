#include "agxv_device.h"

#include "agxv_instance.h"
#include "agxv_physical_device.h"

#include "vulkan/wsi/wsi_common.h"

extern const struct vk_command_buffer_ops agxv_cmd_buffer_ops;

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateDevice(VkPhysicalDevice physicalDevice,
   const VkDeviceCreateInfo *pCreateInfo,
   const VkAllocationCallbacks *pAllocator,
   VkDevice *pDevice)
{
   VK_FROM_HANDLE(agxv_physical_device, physical_device, physicalDevice);
   VkResult result = VK_ERROR_OUT_OF_HOST_MEMORY;
   struct agxv_device *device;

   device = vk_zalloc2(&physical_device->instance->vk.alloc,
      pAllocator,
      sizeof(*device),
      8,
      VK_SYSTEM_ALLOCATION_SCOPE_DEVICE);
   if (!device)
      return vk_error(physical_device, VK_ERROR_OUT_OF_HOST_MEMORY);

   struct vk_device_dispatch_table dispatch_table;
   vk_device_dispatch_table_from_entrypoints(&dispatch_table, &agxv_device_entrypoints, true);
   vk_device_dispatch_table_from_entrypoints(&dispatch_table, &wsi_device_entrypoints, false);

   result =
      vk_device_init(&device->vk, &physical_device->vk, &dispatch_table, pCreateInfo, pAllocator);
   if (result != VK_SUCCESS)
      goto fail_alloc;

   device->pdev = physical_device;
   result = agxv_queue_init(device, &device->queue,
                            &pCreateInfo->pQueueCreateInfos[0], 0);
   if (result != VK_SUCCESS)
      goto fail_alloc;

   device->vk.command_buffer_ops = &agxv_cmd_buffer_ops;
   agx_meta_init(&device->meta_cache, &physical_device->dev, NULL);

   *pDevice = agxv_device_to_handle(device);

   return VK_SUCCESS;

fail_alloc:
   vk_free(&device->vk.alloc, device);
   return result;
}

VKAPI_ATTR void VKAPI_CALL
agxv_DestroyDevice(VkDevice _device, const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);

   if (!device)
      return;

   vk_free(&device->vk.alloc, device);
}
