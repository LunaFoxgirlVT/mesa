#include "agxv_descriptor_pool.h"

#include "agxv_device.h"
#include "agxv_descriptor_set_layout.h"
#include "agxv_physical_device.h"

#include "asahi/lib/agx_device.h"

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateDescriptorPool(VkDevice _device,
                          const VkDescriptorPoolCreateInfo *pCreateInfo,
                          const VkAllocationCallbacks *pAllocator,
                          VkDescriptorPool *pDescriptorPool)
{
   VK_FROM_HANDLE(agxv_device, device, _device);

   VK_MULTIALLOC(ma);
   VK_MULTIALLOC_DECL(&ma, struct agxv_descriptor_pool,
                      pool, 1);
   VK_MULTIALLOC_DECL(&ma, struct agxv_descriptor_pool_entry,
                      entries, pCreateInfo->maxSets);

   pool = vk_object_multizalloc(&device->vk, &ma, pAllocator,
                                VK_OBJECT_TYPE_DESCRIPTOR_POOL);

   if (!pool)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   size_t size = 0;
   for (unsigned i = 0; i < pCreateInfo->poolSizeCount; i++) {
      const VkDescriptorPoolSize *pool_size = &pCreateInfo->pPoolSizes[i];
      size += agxv_binding_stride(pool_size->type) * pool_size->descriptorCount;
   }

   pool->size = size;
   pool->entry_count = pCreateInfo->maxSets;

   pool->bo = agx_bo_create(&device->pdev->dev, size, AGX_BO_WRITEBACK,
                            "Descriptor Pool");
   if (!pool->bo) {
      vk_free(pAllocator, pool);
      return vk_error(device, VK_ERROR_OUT_OF_DEVICE_MEMORY);
   }

   *pDescriptorPool = agxv_descriptor_pool_to_handle(pool);
   return VK_SUCCESS;
}
