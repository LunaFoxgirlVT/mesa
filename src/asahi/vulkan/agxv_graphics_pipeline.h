#ifndef AGXV_GRAPHICS_PIPELINE
#define AGXV_GRAPHICS_PIPELINE 1

#include "agxv_private.h"
#include "agxv_pipeline.h"

#include "vk_graphics_state.h"

struct agxv_graphics_pipeline {
   struct agxv_pipeline base;

   struct vk_dynamic_graphics_state dynamic;

   /* TODO: do we need multiple of these if something stages are different
    * from vertex + fragment?
    */
   struct agx_bo *cf_bindings;

   /* Used to work out what range of buffers to bind on vertex state emission.
    * This is suboptimal if buffer binding locations are not contiguous.
    */
   uint32_t buffer_base;
   uint32_t buffer_len;
};

VK_DEFINE_NONDISP_HANDLE_CASTS(agxv_graphics_pipeline, base.base, VkPipeline,
                               VK_OBJECT_TYPE_PIPELINE);

#endif
