#ifndef AGXV_PIPELINE
#define AGXV_PIPELINE 1

#include "agxv_private.h"

#include "asahi/lib/agx_device.h"
#include "asahi/compiler/agx_compile.h"
#include "agxv_device.h"
#include "vk_pipeline_layout.h"
#include "compiler/nir/nir_builder.h"

enum agxv_pipeline_type {
   AGXV_PIPELINE_GRAPHICS,
   AGXV_PIPELINE_COMPUTE,
};

struct agxv_pipeline {
   struct vk_object_base base;

   enum agxv_pipeline_type type;

   struct {
      struct agx_bo *bo;
      struct agx_shader_info info;
   } stages[MESA_SHADER_STAGES];

   void (*destroy)(struct agxv_pipeline *);
};

VkResult
agxv_compile_pipeline_stage(struct agxv_device *device,
                            struct agxv_pipeline *pipeline,
                            struct vk_pipeline_layout *layout,
                            const VkPipelineShaderStageCreateInfo *pCreateInfo,
                            void (*stage_specific_lowering)(struct agxv_pipeline *pipeline,
                                                            nir_shader *nir,
                                                            struct agx_shader_key *, void *),
                            void *data);

VK_DEFINE_NONDISP_HANDLE_CASTS(agxv_pipeline, base, VkPipeline,
                               VK_OBJECT_TYPE_PIPELINE);

#endif
