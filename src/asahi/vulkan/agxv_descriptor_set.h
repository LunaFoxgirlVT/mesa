#ifndef AGXV_DESCRIPTOR_SET
#define AGXV_DESCRIPTOR_SET 1

#include "agxv_private.h"

#include "agxv_descriptor_pool.h"
#include "agxv_descriptor_set_layout.h"

#include "asahi/lib/agx_bo.h"

struct agxv_descriptor_set {
   struct vk_object_base base;
   struct agxv_descriptor_pool *pool;
   struct agxv_descriptor_set_layout *layout;

   /* index into agxv_descriptor_pool::entries */
   uint32_t entry;
};

static inline uint64_t
agxv_descriptor_set_addr(struct agxv_descriptor_set *set)
{
   return set->pool->bo->ptr.gpu + set->pool->entries[set->entry].offset;
}

struct agxv_buffer_descriptor {
   uint64_t ptr;
   uint32_t off;
   uint32_t zero;
};

VK_DEFINE_HANDLE_CASTS(agxv_descriptor_set, base, VkDescriptorSet,
                       VK_OBJECT_TYPE_DESCRIPTOR_SET)

#endif
