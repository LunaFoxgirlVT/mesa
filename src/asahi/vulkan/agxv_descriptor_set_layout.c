#include "agxv_descriptor_set_layout.h"

#include "agxv_device.h"

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateDescriptorSetLayout(VkDevice _device,
                               const VkDescriptorSetLayoutCreateInfo *pCreateInfo,
                               const VkAllocationCallbacks *pAllocator,
                               VkDescriptorSetLayout *pSetLayout)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   uint32_t num_bindings = 0;
   for (uint32_t i = 0; i < pCreateInfo->bindingCount; i++) {
      const VkDescriptorSetLayoutBinding *binding = &pCreateInfo->pBindings[i];
      num_bindings = MAX2(num_bindings, binding->binding + 1);
   }

   VK_MULTIALLOC(ma);
   VK_MULTIALLOC_DECL(&ma, struct agxv_descriptor_set_layout, layout, 1);
   VK_MULTIALLOC_DECL(&ma, struct agxv_descriptor_set_layout_binding,
                      bindings, num_bindings);

   if (!vk_descriptor_set_layout_multizalloc(&device->vk, &ma))
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   layout->binding_count = num_bindings;

   for (uint32_t i = 0; i < pCreateInfo->bindingCount; i++) {
      const VkDescriptorSetLayoutBinding *binding = &pCreateInfo->pBindings[i];
      bindings[binding->binding].type = binding->descriptorType;
      bindings[binding->binding].array_size = binding->descriptorCount;
   }

   uint32_t offset = 0;
   for (uint32_t i = 0; i < layout->binding_count; i++) {
      /* skip empty bindings */
      if (bindings[i].array_size == 0)
         continue;

      uint32_t stride = agxv_binding_stride(layout->bindings[i].type);
      layout->bindings[i].offset = offset;
      layout->bindings[i].stride = stride;
      offset += stride * layout->bindings[i].array_size;
   };

   layout->size = offset;
   *pSetLayout = agxv_descriptor_set_layout_to_handle(layout);

   return VK_SUCCESS;
}
