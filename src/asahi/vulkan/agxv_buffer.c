#include "agxv_buffer.h"

#include "agxv_device.h"
#include "agxv_device_memory.h"

#include "vulkan/runtime/vk_buffer.h"

VKAPI_ATTR VkResult VKAPI_CALL
agxv_CreateBuffer(VkDevice _device,
                  const VkBufferCreateInfo *pCreateInfo,
                  const VkAllocationCallbacks *pAllocator,
                  VkBuffer *pBuffer)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   struct agxv_buffer *buf;

   buf = vk_buffer_create(&device->vk, pCreateInfo, pAllocator, sizeof(*buf));
   if (!buf)
      return vk_error(device, VK_ERROR_OUT_OF_HOST_MEMORY);

   *pBuffer = agxv_buffer_to_handle(buf);

   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_GetBufferMemoryRequirements2(VkDevice _device,
                                  const VkBufferMemoryRequirementsInfo2 *pInfo,
                                  VkMemoryRequirements2 *pMemoryRequirements)
{
   VK_FROM_HANDLE(agxv_buffer, buffer, pInfo->buffer);
   pMemoryRequirements->memoryRequirements = (VkMemoryRequirements) {
      .size = buffer->vk.size,
      .alignment = 0x10000, /* page size. Don't know if this is correct? */
      .memoryTypeBits = 1
   };

   vk_foreach_struct_const(ext, pMemoryRequirements->pNext) {
      agxv_debug_ignored_stype(ext->sType);
   }
}

VKAPI_ATTR VkResult VKAPI_CALL
agxv_BindBufferMemory2(VkDevice _device,
                       uint32_t bindInfoCount,
                       const VkBindBufferMemoryInfo *pBindInfos)
{
   VK_FROM_HANDLE(agxv_device, device, _device);

   for (uint32_t i = 0; i < bindInfoCount; i++) {
      const VkBindBufferMemoryInfo *info = &pBindInfos[i];
      VK_FROM_HANDLE(agxv_buffer, buffer, info->buffer);
      VK_FROM_HANDLE(agxv_device_memory, mem, info->memory);
      buffer->mem = mem;
      buffer->mem_offset = info->memoryOffset;

      vk_foreach_struct_const(ext, info->pNext) {
         agxv_debug_ignored_stype(ext->sType);
      }
   }

   return VK_SUCCESS;
}

VKAPI_ATTR void VKAPI_CALL
agxv_DestroyBuffer(VkDevice _device,
                   VkBuffer _buffer,
                   const VkAllocationCallbacks *pAllocator)
{
   VK_FROM_HANDLE(agxv_device, device, _device);
   VK_FROM_HANDLE(agxv_buffer, buffer, _buffer);

   if (!buffer)
      return;

   vk_free2(&device->vk.alloc, pAllocator, buffer);
}
