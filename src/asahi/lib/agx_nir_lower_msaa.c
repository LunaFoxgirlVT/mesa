/*
 * Copyright 2022 Alyssa Rosenzweig
 * Copyright © 2021 Intel Corporation
 * SPDX-License-Identifier: MIT
 */

#include "agx_tilebuffer.h"
#include "nir.h"
#include "nir_builder.h"

static bool
lower_to_sample(nir_builder *b, nir_instr *instr, void *data)
{
   nir_ssa_def *sample_id = data;

   if (instr->type != nir_instr_type_intrinsic)
      return false;

   nir_intrinsic_instr *intrin = nir_instr_as_intrinsic(instr);
   b->cursor = nir_before_instr(instr);

   switch (intrin->intrinsic) {
   case nir_intrinsic_load_sample_id:
      nir_ssa_def_rewrite_uses(&intrin->dest.ssa, sample_id);
      nir_instr_remove(instr);
      break;

   case nir_intrinsic_load_sample_pos:
      unreachable("todo");
      break;

   case nir_intrinsic_load_local_pixel_agx:
   case nir_intrinsic_store_local_pixel_agx: {
      unsigned mask_index =
         (intrin->intrinsic == nir_intrinsic_store_local_pixel_agx) ? 1 : 0;

      nir_src *mask_src = &intrin->src[mask_index];
      nir_ssa_def *mask = mask_src->ssa;
      nir_ssa_def *this_sample_mask =
         nir_ishl(b, nir_imm_intN_t(b, 1, 16), nir_u2u32(b, sample_id));

      nir_instr_rewrite_src_ssa(instr, mask_src,
                                nir_iand(b, mask, this_sample_mask));
      break;
   }

   case nir_intrinsic_load_barycentric_sample:
      unreachable("todo");
      /* Lower to at sample */
#if 0
      lowered = nir_load_barycentric(b, nir_intrinsic_load_barycentric_pixel,
                                        nir_intrinsic_interp_mode(intrin));

      if (nir_intrinsic_interp_mode(intrin) == INTERP_MODE_NOPERSPECTIVE) {
         BITSET_SET(b->shader->info.system_values_read,
                    SYSTEM_VALUE_BARYCENTRIC_LINEAR_PIXEL);
      } else {
         BITSET_SET(b->shader->info.system_values_read,
                    SYSTEM_VALUE_BARYCENTRIC_PERSP_PIXEL);
      }
      break;
#endif

   default:
      return false;
   }

   return true;
}

static nir_ssa_def *
wrap_per_sample_loop(nir_shader *shader)
{
   /* Get the original function */
   nir_function_impl *impl = nir_shader_get_entrypoint(shader);

   nir_cf_list list;
   nir_cf_extract(&list, nir_before_block(nir_start_block(impl)),
                  nir_after_block(nir_impl_last_block(impl)));

   /* Create a builder for the wrapped function */
   nir_builder b;
   nir_builder_init(&b, impl);
   b.cursor = nir_after_block(nir_start_block(impl));

   nir_variable *i =
      nir_local_variable_create(impl, glsl_uintN_t_type(16), NULL);
   nir_store_var(&b, i, nir_imm_intN_t(&b, 0, 16), ~0);
   nir_ssa_def *index = NULL;

   /* Create a loop in the wrapped function */
   nir_loop *loop = nir_push_loop(&b);
   {
      index = nir_load_var(&b, i);
      nir_push_if(&b, nir_uge(&b, index, nir_imm_intN_t(&b, 4 /* XXX */, 16)));
      {
         nir_jump(&b, nir_jump_break);
      }
      nir_pop_if(&b, NULL);

      b.cursor = nir_cf_reinsert(&list, b.cursor);
      nir_store_var(&b, i, nir_iadd_imm(&b, index, 1), ~0);
   }
   nir_pop_loop(&b, loop);

   return index;
}

bool
agx_nir_lower_msaa(nir_shader *shader, uint8_t nr_samples)
{
   assert(shader->info.stage == MESA_SHADER_FRAGMENT);
   assert(nr_samples == 1 || nr_samples == 2 || nr_samples == 4);

   /* If multisampling is disabled, we don't need to futz with the control flow
    * of the shader, and can run the more efficient specialized lowering.
    */
   if (nr_samples == 1)
      return nir_lower_single_sampled(shader);

   /* TODO: Something optimized for not per-sample? */

   /* Otherwise, we need to run the shader per-sample. Wrap the shader in a loop
    * over samples, and lower everything accordingly.
    */
   nir_ssa_def *sample_id = wrap_per_sample_loop(shader);
   nir_shader_instructions_pass(
      shader, lower_to_sample,
      nir_metadata_block_index | nir_metadata_dominance, sample_id);

   return true;
}
